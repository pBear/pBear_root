## What is pBear?
pBear (programming Bear) is a IDE for programmers. On the same time it is   
a framwork for developing other IDEs. It is divided in several modules so that  
ever person can chose what part pick for his project.

This module is the Root of all the Projects. Each module depend on the core.  
So the best is install all and then download the part that do you want 
to edit.

If you only want to use the libraries or the IDE then the best is install  
the packet (.dep, .rmp). You can fin its on the man dir of this repository   
or from the main page of this project.

## Soported Platforms
On the moment only linux. It should work also on MAC and FreeBSD.   
Window will be suported. I think i have the time to make is work on Window
this sommer

## why should I use it?
If you want a IDE then the reasin is that it is lighter than eclipse and 
make for suport more than one programming language. 

For developers that want make theyer own IDE it is also the best chose because  
they can focus it give them all the hard things done. So they can focus on  
the iportant parts

## why I make work for the competition?
becuse I belive in the Free Software. IDEs for Free Blatforms need a comun base  
to be a good alternative to the private alternatives. Also I expect some feed 
back.

## How build & install It?
First do you have to install some libraries. I will put the comand for ubuntu  
When I have time I will add the comands for the orhter platforms. libclang-2.8  
can be raplaced with other version but then you have to change it also in the  
cmake file.

    sudo apt-get install libclang-3.8
    sudo apt-get install libboost-log
    sudo apt-get install libboost-serialization
    sudo apt-get install libboost-system
    sudo apt-get install libboost-filesystem
    sudo apt-get install libboost-thread
    sudo apt-get install libclang
    sudo apt-get install libboost-regex

If you dont have installed qt5

    sudo apt-get install libqt5core5a
    sudo apt-get install libqt5gui5
    sudo apt-get install libqt5gui5-gles
    sudo apt-get install libqt5widgets5
   
Boost spirit ist too old on ubuntu 16.04.

    git clone https://github.com/boostorg/spirit.git
    cd spirit
    sudo cp include/boost /usr/boost

Now download it with all supmodules and install it. It can take some  
time.

    git clone --recursive git@gitlab.com:pBear/pBear_root.git
    cd pBear_root
    cmake .
    make
    sudo make install
 
## Note: Fix some special cases
 If you dont can install libclang-3.8 then install other version and change the 
 cmake files. E.g libclang-3.6:
 
     sed -i 's/llvm-3.8/llvm-3.6/g' *
    