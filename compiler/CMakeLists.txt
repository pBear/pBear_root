project(pBear_compiler)
cmake_minimum_required(VERSION 2.8)

find_package(Boost REQUIRED COMPONENTS regex system thread serialization)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_CXX_FLAGS "-std=c++14 ${CMAKE_CXX_FLAGS}")

include_directories(${CMAKE_BINARY_DIR}/TerminalProcess/client)
link_directories(${CMAKE_BINARY_DIR}/libs)

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/libs)

add_library(pBear_compiler SHARED  gramar.cpp terminalprocessconnector.cpp)


target_link_libraries(pBear_compiler -lboost_regex -lpBear_termical_process -lboost_system -lboost_thread -lpthread -lboost_serialization)

INSTALL( TARGETS pBear_compiler LIBRARY DESTINATION lib )
