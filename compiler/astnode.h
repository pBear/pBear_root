#ifndef ASTNODEDEBUGER_H
#define ASTNODEDEBUGER_H

#include <memory>
#include <list>

#include <iostream>
#include <vector>

#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/variant/recursive_variant.hpp>



struct Location
{
    std::string file;
    int         line = -1;
    int         col  = -1;
};

struct Included;
struct IsueBlock;

struct Isue
{
    enum Type {ERROR, WARNING, NOTE, TEXTLOCATION};
    Location                 location;
    Type                     type = TEXTLOCATION;
    std::string              text;
    std::vector<std::string> aditionallines;
    std::vector<Included>    includes;
    std::vector<Isue>        isue;
};

struct IsueBlock
{
    std::vector<Isue> isues;
};

struct Included
{
    std::vector<Location> froms;
    Isue                  isue;
};

struct Root
{
    std::vector<Included>    includes;
    std::vector<Isue>        isues;
    std::vector<std::string> no_recognized_lines;
    std::string              befor_text;
};

BOOST_FUSION_ADAPT_STRUCT(
    Location,
    (std::string, file)
    (int        , line)
    (int        , col)
)

BOOST_FUSION_ADAPT_STRUCT(
    Isue,
    (Location                , location)
    (Isue::Type              , type)
    (std::string             , text)
    (std::vector<std::string>, aditionallines)
    (std::vector<Included>   , includes)
    (std::vector<Isue>       , isue)
)

BOOST_FUSION_ADAPT_STRUCT(
    IsueBlock,
    (std::vector<Isue>, isues)
)

BOOST_FUSION_ADAPT_STRUCT(
    Included,
    (std::vector<Location>, froms)
    (Isue                 , isue)
)

BOOST_FUSION_ADAPT_STRUCT(
    Root,
    (std::vector<Included>,    includes)
    (std::vector<Isue>,        isues)
    (std::vector<std::string>, no_recognized_lines)
    (std::string,              befor_text)
)

#endif // ASTNODE_H
