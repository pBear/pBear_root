﻿#include "gramar.h"

#include <memory>

#include <boost/spirit/include/qi.hpp>
#include <boost/variant.hpp>
#include <boost/spirit/include/qi_lexeme.hpp>
#include <boost/spirit/include/support_utree.hpp>

#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/spirit/include/phoenix_object.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/variant/recursive_variant.hpp>
#include <boost/foreach.hpp>
#include <boost/regex.hpp>

using namespace boost::spirit;
using Iterator = std::string::iterator;

namespace pBear {
namespace compiler {


struct my_grammar : public boost::spirit::qi::grammar<Iterator, Root()>
{
  boost::spirit::qi::rule<Iterator, Root()>                             root;
  boost::spirit::qi::rule<Iterator, Isue()>                             isue;
  boost::spirit::qi::rule<Iterator, Isue()>                             isue_note;
  boost::spirit::qi::rule<Iterator, Location()>                         location;
  boost::spirit::qi::rule<Iterator, boost::spirit::utree()>             error;
  boost::spirit::qi::rule<Iterator, boost::spirit::utree()>             warning;
  boost::spirit::qi::rule<Iterator, boost::spirit::utree()>             note;
  boost::spirit::qi::rule<Iterator, boost::spirit::utree()>             endline;
  boost::spirit::qi::rule<Iterator, Included()>                         included;

  boost::spirit::qi::rule<Iterator, std::string()>                       string_no_simble_col_boost;
  boost::spirit::qi::rule<Iterator, std::string()>                       number_boost;
  boost::spirit::qi::rule<Iterator, std::string()>                       text_boost;
  boost::spirit::qi::rule<Iterator, std::string()>                       aditionalline_boost;
  boost::spirit::qi::rule<Iterator, std::string()>                       line;

  my_grammar() : my_grammar::base_type(root)
  {
    using boost::phoenix::push_back;
    using boost::phoenix::at_c;

    root                       = *(included[push_back(at_c<0>(_val), _1)] | isue[push_back(at_c<1>(_val), _1)] | line[push_back(at_c<2>(_val), _1)]);
    isue                       = location[at_c<0>(_val) = _1] >>  ":" >> -(error[at_c<1>(_val) = Isue::ERROR] | warning[at_c<1>(_val)  = Isue::WARNING] | note[at_c<1>(_val)  = Isue::NOTE])  >>
                                  text_boost[at_c<2>(_val) = _1]  >> ((endline >> +(aditionalline_boost[push_back(at_c<3>(_val), _1)] >> endline)) | (":" >> endline >> +(included[push_back(at_c<4>(_val), _1)] | isue[push_back(at_c<5>(_val), _1)])));
    isue_note                  = location[at_c<0>(_val) = _1] >>  ":" >> note[at_c<1>(_val)  = Isue::NOTE]  >>
                                  text_boost[at_c<2>(_val) = _1]  >> ((endline >> +(aditionalline_boost[push_back(at_c<3>(_val), _1)] >> endline)) | (":" >> endline >> (included[push_back(at_c<4>(_val), _1)] | isue[push_back(at_c<5>(_val), _1)])));
    location                   = /*file[push_back(_val, _1)]*/ string_no_simble_col_boost[at_c<0>(_val) =  _1] >> -(":"  >> qi::int_[at_c<1>(_val) =  _1] >> -(":" >> qi::int_[at_c<2>(_val) = _1]));
    error                      = *qi::char_(" ") >> "error" >> ":";
    warning                    = *qi::char_(" ") >> "warning" >> ":";
    note                       = *qi::char_(" ") >> "note" >> ":";
    aditionalline_boost        = +qi::char_(" ") >> *(qi::char_ - "\n" - "\r\n");
    string_no_simble_col_boost = +(qi::char_ - ":" - "\n" - "\r\n" - "In file included");
    text_boost                 = *(qi::char_ - "\n" - "\r\n" - ":\n" - ":\r\n");
    endline                    = qi::char_("\n") | "\r\n";
    line                       = (+(qi::char_ - "\n" - "\r\n") >> -qi::omit[endline]) | (*(qi::char_ - "\n" - "\r\n") >> qi::omit[endline]);
    included                   = "In file included" >> +(qi::omit[+qi::char_(" ") >> "from" >> +qi::char_(" ")] >> location[push_back(at_c<0>(_val), _1)] >> qi::omit[qi::char_(":") | ","] >> endline) >> isue[at_c<1>(_val) = _1];

    root.name("root");
    isue.name("isue");
    location.name("location");
    line.name("line");
    error.name("error");
    warning.name("warning");
    note.name("note");
    string_no_simble_col_boost.name("string_no_simble_col_boost");
    number_boost.name("number_boost");
    text_boost.name("text_boost");
    endline.name("endline");
    included.name("included");

    qi::on_error<qi::fail>
    (
        root
      , std::cout
            << boost::phoenix::val("Error! Expecting ")
            << _4                               // what failed?
            << boost::phoenix::val(" here: \"")
            << boost::phoenix::construct<std::string>(_3, _2)   // iterators to error-pos, end
            << boost::phoenix::val("\"")
            << std::endl
    );
  }
};

struct LineCheck : public boost::spirit::qi::grammar<Iterator, boost::spirit::utree()>
{
  boost::spirit::qi::rule<Iterator, boost::spirit::utree()>             root;
  boost::spirit::qi::rule<Iterator, boost::spirit::utree()>             isue;
  boost::spirit::qi::rule<Iterator, boost::spirit::utree()>             location;
  boost::spirit::qi::rule<Iterator, boost::spirit::utree()>             error;
  boost::spirit::qi::rule<Iterator, boost::spirit::utree()>             warning;
  boost::spirit::qi::rule<Iterator, boost::spirit::utree()>             note;
  boost::spirit::qi::rule<Iterator, boost::spirit::utree()>             included;

  boost::spirit::qi::rule<Iterator, std::string()>                       string_no_simble_col_boost;
  boost::spirit::qi::rule<Iterator, std::string()>                       number_boost;
  boost::spirit::qi::rule<Iterator, std::string()>                       text_boost;
  boost::spirit::qi::rule<Iterator, std::string()>                       aditionalline_boost;

  LineCheck() : LineCheck::base_type(root)
  {
    using boost::phoenix::push_back;
    using boost::phoenix::at_c;

    root                       = +(included | isue) >> (qi::char_("\n")|qi::char_("\r\n"));
    isue                       = location >>  ":" >> *(error | warning | note)  >> text_boost  >> qi::char_(":");
    location                   = +(qi::char_ - ":" - "\n" - "\r\n" - "In file included") >> -(":"  >> qi::int_ >> -(":" >> qi::int_));
    error                      = *qi::char_(" ") >> "error" >> ":";
    warning                    = *qi::char_(" ") >> "warning" >> ":";
    note                       = *qi::char_(" ") >> "note" >> ":";
    text_boost                 = *(qi::char_ - "\n" - "\r\n" - ":\n" - ":\r\n");
    included                   = "In file included" >> +qi::char_(" ") >> "from" >> +qi::char_(" ") >> location >> (qi::char_(":") | ",");

    root.name("root");
    isue.name("isue");
    location.name("location");
    error.name("error");
    warning.name("warning");
    note.name("note");
    text_boost.name("text_boost");
    included.name("included");

    qi::on_error<qi::fail>
    (
        root
      , std::cout
            << boost::phoenix::val("Error! Expecting ")
            << _4                               // what failed?
            << boost::phoenix::val(" here: \"")
            << boost::phoenix::construct<std::string>(_3, _2)   // iterators to error-pos, end
            << boost::phoenix::val("\"")
            << std::endl
    );
  }
};

bool isValidFirstLine(std::string::iterator begin, std::string::iterator end)
{
    LineCheck g;
    return qi::parse(begin, end, g);
}

std::shared_ptr<Root> parse(std::string text)
{
    text = boost::regex_replace(text, boost::regex(R"(\x1B\[([\d;])*(m|K))"), "");

    my_grammar g;

    std::shared_ptr<Root> v = std::make_shared<Root>();

    std::string::iterator begin_iterator = text.begin();
    std::string::iterator end_iterator   = text.begin();

    do
    {
        if (begin_iterator != end_iterator)
        {
            begin_iterator = end_iterator;
        }
        end_iterator = std::find_if(begin_iterator, text.end(), [](char c){return c == '\n';});
        end_iterator++;
    }
    while (begin_iterator != text.end() && !isValidFirstLine(begin_iterator, end_iterator));

    v->befor_text = std::string(text.begin(), begin_iterator);

    if (begin_iterator != text.end())
    {
        qi::parse(begin_iterator, text.end(), g, *v);
    }
    return v;
}

void addIsue(std::vector<Isue*> &vec, Isue *isue);

void addInclude(std::vector<Isue*> &vec, Included *included)
{
    addIsue(vec, &included->isue);
}

void addIsue(std::vector<Isue*> &vec, Isue *isue)
{
    vec.push_back(isue);
    for (Included &x: isue->includes)
    {
        addInclude(vec, &x);
    }
    for (Isue &x: isue->isue)
    {
        addIsue(vec, &x);
    }
}

std::vector<Isue *> getIsues(std::shared_ptr<Root> root)
{
    std::vector<Isue*> isues;
    for (Included &x: root->includes)
    {
        addInclude(isues, &x);
    }
    for (Isue &x: root->isues)
    {
        addIsue(isues, &x);
    }
    return isues;
}

}
}
