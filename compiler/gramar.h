#ifndef GRAMAR_H
#define GRAMAR_H

#include "astnode.h"

#include <string>
#include <memory>

namespace pBear {
namespace compiler {

enum node
{
    ROOT                  = 0,
    ISUE_BLOCK            = 1,
    ISUE                  = 2,
    LOCATION              = 3,
    FILE                  = 4,
    LINE                  = 5,
    COLUMN                = 6,
    SIMPLE_LOCATION       = 7,
    ERROR                 = 8,
    WARNING               = 9,
    NOTE                  = 10,
    ADITIONALLINE         = 11,
    STRING_NO_SIMBLE_COL  = 12,
    NUMBER                = 13,
    TEXT                  = 14,
    ENDLINE               = 15,
    INCLUDED              = 16,
    FROM                  = 17,
};

std::shared_ptr<Root> parse(std::string text);
bool                  isValidFirstLine(std::string::iterator begin, std::string::iterator end);
std::vector<Isue*>    getIsues(std::shared_ptr<Root> root);

}
}

#endif // GRAMAR_H
