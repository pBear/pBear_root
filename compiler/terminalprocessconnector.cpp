#include "terminalprocessconnector.h"
#include "gramar.h"

#include <unistd.h>
#include <boost/regex.hpp>

using namespace pBear::compiler;
using namespace pBear::ConnectionClient;

std::vector<std::function<void(std::string, std::shared_ptr<Root>, std::shared_ptr<Process>)>> TerminalProcessConnector::funs;

TerminalProcessConnector::TerminalProcessConnector(std::string comand)
{
    this->comand = comand;
}

TerminalProcessConnector::~TerminalProcessConnector()
{
    std::cout << "destructor" << std::endl;
}

void TerminalProcessConnector::addFunction(std::function<void(std::string, std::shared_ptr<Root>, std::shared_ptr<pBear::ConnectionClient::Process>)> fun)
{
    funs.push_back(fun);
}

std::shared_ptr<Connection> TerminalProcessConnector::getConnection()
{
    return conn;
}

void TerminalProcessConnector::start(std::shared_ptr<Process> pro, std::shared_ptr<Connection> con)
{
    this->conn = con;
    //this->pro  = pro;

    Process_server::remoteDisplay(true, con);
    Process_server::makeProcess(comand, con);
}

void TerminalProcessConnector::run(std::string data, ProcessData::Type proData)
{
    buffer.append(data);
}

bool TerminalProcessConnector::end()
{
    std::shared_ptr<Root> root = parse(buffer);
    for (auto fun: funs)
    {
        if (fun)
        {
            fun(buffer, root, pro);
        }
    }
    return root->isues.empty() && root->includes.empty() && root->no_recognized_lines.empty();
}

TerminalProcessConnector::ComandType TerminalProcessConnector::getComandTpye()
{
    if (comand.empty()) return UNUSABLE;
    if (boost::regex_search(comand, boost::regex(R"(^g\+\+|^gcc)")))
    {
        return VALID;
    }
    return UNCNOWN;
}
