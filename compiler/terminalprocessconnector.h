#ifndef TERMINALPROCESSCONNECTOR_H
#define TERMINALPROCESSCONNECTOR_H

#include "astnode.h"

#include "/home/aron/pBear/TerminalProcess/client/process_server.h"
#include "/home/aron/pBear/TerminalProcess/client/ProcessQueneElement_Interface.h"

#include <functional>

namespace pBear
{
    namespace compiler
    {
        class TerminalProcessConnector: public ConnectionClient::ProcessQueneElement_Interface
        {
        private:
            std::string                                buffer;
            static std::vector<std::function<void(std::string, std::shared_ptr<Root>, std::shared_ptr<ConnectionClient::Process>)>> funs;
            std::shared_ptr<ConnectionClient::Connection>                conn;
            std::string                                comand;
            std::shared_ptr<ConnectionClient::Process> pro;
        public:
            enum ComandType{VALID, UNCNOWN, UNUSABLE};
            TerminalProcessConnector(std::string comand);
            ~TerminalProcessConnector();
            static void addFunction(std::function<void(std::string, std::shared_ptr<Root>, std::shared_ptr<pBear::ConnectionClient::Process>)>fun);
            std::shared_ptr<ConnectionClient::Connection> getConnection();
            void start(std::shared_ptr<ConnectionClient::Process> pro, std::shared_ptr<ConnectionClient::Connection> con);
            void run(std::string data, ProcessData::Type proData);
            bool end();
            ComandType getComandTpye();
        };
    }
}

#endif // TERMINALPROCESSCONNECTOR_H
