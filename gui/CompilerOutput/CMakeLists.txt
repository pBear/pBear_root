cmake_minimum_required(VERSION 2.8.3)

project(pBear_gui_CompilerOutput)

# Tell CMake to run moc when necessary:
set(CMAKE_AUTOMOC ON)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

# Find the QtWidgets library
find_package(Qt5Widgets REQUIRED)

# Add the include directories for the Qt 5 Widgets module to
# the compile lines.
include_directories(${Qt5Widgets_INCLUDE_DIRS})

# Use the compile definitions defined in the Qt 5 Widgets module
add_definitions(${Qt5Widgets_DEFINITIONS})

# Add compiler flags for building executables (-fPIE)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${Qt5Widgets_EXECUTABLE_COMPILE_FLAGS} -std=c++14")


file(GLOB src_files "*.cpp")
file(GLOB src_exclude "*moc*")
file(GLOB src_ui      "*.ui")
list(REMOVE_ITEM src_files ${src_exclude} "aaaaaa")

QT5_WRAP_UI(UIS_HDRS ${src_ui})

include_directories("../../corre/include" 
	"../../ExecuteDebugCompileTerminal" 
	"../../compiler" 
	"../../Execute" 
	"../../debuger"
        "../Dialogs" 
	"../../TerminalProcess/client/")

link_directories(../../libs)

add_library(pBear_gui_CompilerOutput SHARED ${src_files} ${UIS_HDRS})

target_link_libraries(pBear_gui_CompilerOutput -lpBear_corre -lboost_filesystem 
	-lboost_system -lboost_regex -lpBear_executedebugcompileterminal 
	-lboost_filesystem -lboost_system -lpBear_compiler -lpBear_corre 
	-lboost_regex -lpthread  -lboost_regex -lpBear_termical_process 
        -lboost_system -lboost_thread -lpthread -lboost_serialization 
	-lpBear_termical_process -lpBear_execute -lpBear_debuger -ldl
	${Qt5Widgets_LIBRARIES}
)

INSTALL(TARGETS pBear_gui_CompilerOutput LIBRARY DESTINATION lib)
