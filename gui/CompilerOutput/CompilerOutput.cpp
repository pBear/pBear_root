#include "CompilerOutput.h"
#include "ui_CompilerOutputs.h"

#include "terminalprocessconnector.h"
#include "gramar.h"
#include "event.h"

#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <functional>

#include <QFontDatabase>
#include <QLabel>
#include <QMenu>

std::string CompilerOutput::parseAscii(std::string text)
{
    return boost::regex_replace(text, boost::regex(R"(\x1B\[([\d;])*(m|K))"), "");
}

void CompilerOutput::addIsue(QTreeWidgetItem *item, Isue isue)
{
    std::string isueType;
    switch (isue.type) {
    case Isue::ERROR:
        isueType = " ERROR";
        break;
    case Isue::WARNING:
        isueType = " WARNING";
        break;
    case Isue::NOTE:
        isueType = " NOTE";
        break;
    case Isue::TEXTLOCATION:
        isueType = " TEXTLOCATION";
        break;
    default:
        break;
    }
    item->setText(0, (parseAscii(isue.location.file) + ":" +
                      ((isue.location.line >= 0)? std::to_string(isue.location.line) +
                      ((isue.location.col >= 0)?":" + std::to_string(isue.location.col) + ":": ""): "") + isueType +
                      std::string(":") + isue.text).c_str());

    /*if (!isue.text.empty())
    {
        QTreeWidgetItem *tmp = new QTreeWidgetItem;
        item->addChild(tmp);
        tmp->setText(0, parseAscii(isue.text).c_str());
    }*/

    for (Isue x: isue.isue)
    {
        QTreeWidgetItem *tmp = new QTreeWidgetItem;
        item->addChild(tmp);
        addIsue(tmp, x);
    }

    for (std::string x: isue.aditionallines)
    {
        QTreeWidgetItem *tmp = new QTreeWidgetItem;
        tmp->setText(0, x.c_str());
        item->addChild(tmp);
    }

    for (Included x: isue.includes)
    {
        QTreeWidgetItem *tmp = new QTreeWidgetItem;
        addIncluded(tmp, x);
        item->addChild(tmp);
    }
}

void CompilerOutput::addIncluded(QTreeWidgetItem *item, Included included)
{
    item->setText(0, "Included");
    for (Location loc: included.froms)
    {
        QTreeWidgetItem *tmp = new QTreeWidgetItem;
        tmp->setText(0, ("From: " + parseAscii(loc.file) + ":" +
                        ((loc.line >= 0)? std::to_string(loc.line) +
                        ((loc.col >= 0)?":" + std::to_string(loc.col): ""): "")).c_str());
        item->addChild(tmp);
    }
    QTreeWidgetItem *tmp = new QTreeWidgetItem;
    addIsue(tmp, included.isue);
    item->addChild(tmp);
}

CompilerOutput::CompilerOutput(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CompilerOutput)
{
    ui->setupUi(this);

    qRegisterMetaType<std::string>();
    qRegisterMetaType<std::shared_ptr<Root>>();
    qRegisterMetaType<std::shared_ptr<pBear::ConnectionClient::Process>>();

    connect(this, SIGNAL(threadChange(std::string, std::shared_ptr<Root>, std::shared_ptr<pBear::ConnectionClient::Process>)), this, SLOT(changeMainThread(std::string, std::shared_ptr<Root>, std::shared_ptr<pBear::ConnectionClient::Process>)));

    ui->TextOutput->setReadOnly(true);
    ui->TextOutput->setLineWrapMode(QTextEdit::NoWrap);

    pBear::compiler::TerminalProcessConnector::addFunction(std::bind(&CompilerOutput::change,
                                                           this, std::placeholders::_1,
                                                           std::placeholders::_2,
                                                           std::placeholders::_3));
}

void CompilerOutput::change(std::string text, std::shared_ptr<Root> root, std::shared_ptr<pBear::ConnectionClient::Process> pro)
{
    //ui->TextOutput->setPlainText(text.c_str());
    std::string result = text;
    result = boost::regex_replace(text, boost::regex(R"(\x1B\[([\d;])*(m|K))"), "");
    emit threadChange(result, root, pro);
}

CompilerOutput::~CompilerOutput()
{
    delete ui;
}

void CompilerOutput::changeMainThread(std::string text, std::shared_ptr<Root> root, std::shared_ptr<pBear::ConnectionClient::Process>)
{
    ui->TextOutput->setPlainText(QString::fromStdString(text));
    ui->treeWidget->clear();
    ui->listWidget->clear();

    //---------------------------------------------------------------
    QTreeWidgetItem *beforText           = new QTreeWidgetItem;
    beforText->setText(0, "befor text");
    std::vector<std::string> beforVec;
    boost::split(beforVec, root->befor_text, boost::is_any_of("\n"));
    for (std::string line: beforVec)
    {
        if (line != "")
        {
            QTreeWidgetItem *tmp = new QTreeWidgetItem;
            tmp->setText(0, line.c_str());
            beforText->addChild(tmp);
        }
    }

    ui->treeWidget->addTopLevelItem(beforText);

    QTreeWidgetItem *includes = new QTreeWidgetItem;
    includes->setText(0, "Includes");
    for (Included x: root->includes)
    {
        QTreeWidgetItem *item = new QTreeWidgetItem;
        addIncluded(item, x);
        includes->addChild(item);
    }
    ui->treeWidget->addTopLevelItem(includes);

    QTreeWidgetItem *isues = new QTreeWidgetItem;
    isues->setText(0, "Isues");
    for (Isue x: root->isues)
    {
        QTreeWidgetItem *item = new QTreeWidgetItem;
        addIsue(item, x);
        isues->addChild(item);
    }
    ui->treeWidget->addTopLevelItem(isues);


    QTreeWidgetItem *noRecognizedItem = new QTreeWidgetItem;
    noRecognizedItem->setText(0, "No recognized lines");
    for (std::string x: root->no_recognized_lines)
    {
        QTreeWidgetItem *item = new QTreeWidgetItem;
        item->setText(0, parseAscii(x).c_str());
        noRecognizedItem->addChild(item);
    }
    ui->treeWidget->addTopLevelItem(noRecognizedItem);

    //------------------------------------------------------------
    for (Isue *isue :pBear::compiler::getIsues(root))
    {
        std::string isueType;
        switch (isue->type) {
        case Isue::ERROR:
            isueType = " ERROR";
            break;
        case Isue::WARNING:
            isueType = " WARNING";
            break;
        case Isue::NOTE:
            isueType = " NOTE";
            break;
        case Isue::TEXTLOCATION:
            isueType = " TEXTLOCATION";
            break;
        default:
            break;
        }
        ui->listWidget->addItem((parseAscii(isue->location.file) + ":" +
                          ((isue->location.line >= 0)? std::to_string(isue->location.line) +
                          ((isue->location.col >= 0)?":" + std::to_string(isue->location.col) + ":": ""): "") + isueType +
                                 std::string(":") + isue->text).c_str());
    }
}

extern "C" {
    QWidget *getWidget(std::shared_ptr<pBear::corre::Event> event)
    {
        return new CompilerOutput;
    }
    std::string type()
    {
        return "QDockWidget";
    }
    std::string orientation()
    {
        return "bottom";
    }
    QMenu* getMenu(std::shared_ptr<pBear::corre::Event> event)
    {
        return new QMenu("Compiler Output");
    }
}
