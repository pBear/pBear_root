#ifndef WIDGET_H
#define WIDGET_H



#include <QWidget>
#include <QTextFormat>
#include <QTextEdit>
#include <QTreeWidgetItem>

namespace Ui {
class CompilerOutput;
}

#include "process.h"
#include "astnode.h"

Q_DECLARE_METATYPE(std::string)
Q_DECLARE_METATYPE(std::shared_ptr<Root>)
Q_DECLARE_METATYPE(std::shared_ptr<pBear::ConnectionClient::Process>)

class CompilerOutput : public QWidget
{
    Q_OBJECT
private:
    std::string parseAscii(std::string text);
    void addIsue(QTreeWidgetItem *item, Isue isue);
    void addIncluded(QTreeWidgetItem *item, Included included);
public:
    explicit CompilerOutput(QWidget *parent = 0);
    void change(std::string, std::shared_ptr<Root>, std::shared_ptr<pBear::ConnectionClient::Process>);
    ~CompilerOutput();
private slots:
    void changeMainThread(std::string, std::shared_ptr<Root>, std::shared_ptr<pBear::ConnectionClient::Process>);
signals:
    void threadChange(std::string, std::shared_ptr<Root>, std::shared_ptr<pBear::ConnectionClient::Process>);
private:
    Ui::CompilerOutput *ui;
};

#endif // WIDGET_H
