/********************************************************************************
** Form generated from reading UI file 'CompilerOutputs.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_COMPILEROUTPUTS_H
#define UI_COMPILEROUTPUTS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CompilerOutput
{
public:
    QVBoxLayout *verticalLayout_4;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_3;
    QTextEdit *TextOutput;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_2;
    QTreeWidget *treeWidget;
    QWidget *tab_3;
    QVBoxLayout *verticalLayout;
    QListWidget *listWidget;

    void setupUi(QWidget *CompilerOutput)
    {
        if (CompilerOutput->objectName().isEmpty())
            CompilerOutput->setObjectName(QStringLiteral("CompilerOutput"));
        CompilerOutput->resize(400, 300);
        verticalLayout_4 = new QVBoxLayout(CompilerOutput);
        verticalLayout_4->setSpacing(0);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        tabWidget = new QTabWidget(CompilerOutput);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        verticalLayout_3 = new QVBoxLayout(tab);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        TextOutput = new QTextEdit(tab);
        TextOutput->setObjectName(QStringLiteral("TextOutput"));

        verticalLayout_3->addWidget(TextOutput);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        verticalLayout_2 = new QVBoxLayout(tab_2);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        treeWidget = new QTreeWidget(tab_2);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setText(0, QStringLiteral("1"));
        treeWidget->setHeaderItem(__qtreewidgetitem);
        treeWidget->setObjectName(QStringLiteral("treeWidget"));

        verticalLayout_2->addWidget(treeWidget);

        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        verticalLayout = new QVBoxLayout(tab_3);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        listWidget = new QListWidget(tab_3);
        listWidget->setObjectName(QStringLiteral("listWidget"));

        verticalLayout->addWidget(listWidget);

        tabWidget->addTab(tab_3, QString());

        verticalLayout_4->addWidget(tabWidget);


        retranslateUi(CompilerOutput);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(CompilerOutput);
    } // setupUi

    void retranslateUi(QWidget *CompilerOutput)
    {
        CompilerOutput->setWindowTitle(QApplication::translate("CompilerOutput", "Widget", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("CompilerOutput", "Output", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("CompilerOutput", "Tree", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("CompilerOutput", "Isues", 0));
    } // retranslateUi

};

namespace Ui {
    class CompilerOutput: public Ui_CompilerOutput {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_COMPILEROUTPUTS_H
