#-------------------------------------------------
#
# Project created by QtCreator 2015-12-28T22:20:35
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS += -std=c++14

TARGET = DebugerListFunctions
TEMPLATE      = lib


SOURCES +=  debugerlistfunctions.cpp

HEADERS  += debugerlistfunctions.h

FORMS    += debugerlistfunctions.ui

LIBS += -L../../libs -lpBear_corre -lboost_filesystem -lboost_system -lboost_regex -lpBear_executedebugcompileterminal

INCLUDEPATH += ../../corre/include ../../ExecuteDebugCompileTerminal ../../compiler ../../Execute ../../debuger /home/aron/pBear/TerminalProcess/client/
DEPENDPATH  += ../../libs
