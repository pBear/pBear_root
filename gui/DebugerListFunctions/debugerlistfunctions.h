#ifndef DEBUGERLISTFUNCTIONS_H
#define DEBUGERLISTFUNCTIONS_H

#include <process.h>
#include <terminalprocessconnectordebuger.h>

#include <QWidget>
#include <QTreeWidgetItem>

namespace Ui {
class DebugerListFunctions;
}

Q_DECLARE_METATYPE(pBear::debuger::Frame)
Q_DECLARE_METATYPE(std::list<pBear::debuger::Frame>)
Q_DECLARE_METATYPE(std::shared_ptr<pBear::ConnectionClient::Process>)

class DebugerListFunctions : public QWidget
{
    Q_OBJECT
private:
    std::shared_ptr<pBear::ConnectionClient::Process> pro;
    pBear::debuger::TerminalProcessConnectorDebuger  *deb;
public:
    explicit DebugerListFunctions(QWidget *parent = 0);
    ~DebugerListFunctions();
    void start(std::shared_ptr<pBear::ConnectionClient::Process>, pBear::debuger::TerminalProcessConnectorDebuger *deb);
    void end(std::shared_ptr<pBear::ConnectionClient::Process>, pBear::debuger::TerminalProcessConnectorDebuger *deb);
    void listEvent(std::list<pBear::debuger::Frame> frames);
    void currFrameEvent(pBear::debuger::Frame frame);
private slots:
    void itemDoubleClicked(QTreeWidgetItem * item, int column);
    void slotListEvent(std::list<pBear::debuger::Frame>);
    void slotCurrFrameEvent(pBear::debuger::Frame);
    void slotQtEnd(std::shared_ptr<pBear::ConnectionClient::Process>, pBear::debuger::TerminalProcessConnectorDebuger *deb);
signals:
    void signallListEvent(std::list<pBear::debuger::Frame>);
    void signallCurrFrameEvent(pBear::debuger::Frame);
    void signalQtEnd(std::shared_ptr<pBear::ConnectionClient::Process>, pBear::debuger::TerminalProcessConnectorDebuger *deb);
private:
    Ui::DebugerListFunctions *ui;
};

#endif // DEBUGERLISTFUNCTIONS_H
