#-------------------------------------------------
#
# Project created by QtCreator 2015-10-14T03:43:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DebugerOutput
#TEMPLATE = app
TEMPLATE      = lib

QMAKE_CXXFLAGS += -std=c++14 -Wl,-R/home/aron/pBear/gui/build-debug -g

SOURCES += main.cpp\
        debugeroutput.cpp \
    debugeroutputtab.cpp

HEADERS  += debugeroutput.h \
    model.h \
    debugeroutputtab.h

FORMS    += debugeroutput.ui \
    debugeroutputtab.ui


LIBS += -L../../libs -lpBear_corre -lboost_filesystem -lboost_system -lboost_regex -lpBear_executedebugcompileterminal

LIBS += -L/home/aron/pBear/libs   -lboost_system -lboost_thread -lpthread -lboost_serialization -lpBear_termical_process -lpBear_execute -lpBear_debuger
LIBS += -lpBear_compiler -lpBear_corre -lboost_regex -lpthread  -lboost_regex -lpBear_termical_process -lboost_system -lboost_thread -lpthread -lboost_serialization

INCLUDEPATH += ../../corre/include ../../ExecuteDebugCompileTerminal ../../compiler ../../Execute ../../debuger /home/aron/pBear/TerminalProcess/client/
DEPENDPATH  += ../../libs
