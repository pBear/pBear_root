#include "debugeroutputtab.h"
#include "ui_debugeroutputtab.h"

#include "debugeroutput.h"


#include <QMenu>

DebugerOutputTab::DebugerOutputTab(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DebugerOutputTab)
{
    ui->setupUi(this);

    qRegisterMetaType<std::shared_ptr<pBear::ConnectionClient::Process>>();
    qRegisterMetaType<pBear::debuger::TerminalProcessConnectorDebuger*>();

    pBear::debuger::TerminalProcessConnectorDebuger::addFunctionStart(std::bind(&DebugerOutputTab::start, this, std::placeholders::_1, std::placeholders::_2));
    pBear::debuger::TerminalProcessConnectorDebuger::addFunctionEnd(std::bind(&DebugerOutputTab::end, this, std::placeholders::_1, std::placeholders::_2));

    connect(this, SIGNAL(startQSignal(std::shared_ptr<pBear::ConnectionClient::Process>,pBear::debuger::TerminalProcessConnectorDebuger*)),
            this, SLOT(startQt(std::shared_ptr<pBear::ConnectionClient::Process>,pBear::debuger::TerminalProcessConnectorDebuger*)));
    connect(this, SIGNAL(endQtSignal(std::shared_ptr<pBear::ConnectionClient::Process>,pBear::debuger::TerminalProcessConnectorDebuger*)),
            this, SLOT(endQt(std::shared_ptr<pBear::ConnectionClient::Process>,pBear::debuger::TerminalProcessConnectorDebuger*)));
}

DebugerOutputTab::~DebugerOutputTab()
{
    delete ui;
}

void DebugerOutputTab::start(std::shared_ptr<pBear::ConnectionClient::Process> pro, pBear::debuger::TerminalProcessConnectorDebuger *deb)
{
    emit startQSignal(pro, deb);
}

void DebugerOutputTab::end(std::shared_ptr<pBear::ConnectionClient::Process> pro, pBear::debuger::TerminalProcessConnectorDebuger *deb)
{
    emit endQtSignal(pro, deb);
}

void DebugerOutputTab::startQt(std::shared_ptr<pBear::ConnectionClient::Process> pro, pBear::debuger::TerminalProcessConnectorDebuger *deb)
{
    DebugerOutput *wid = new DebugerOutput;
    ui->tabWidget->addTab(wid, "Output");
    wid->start(pro, deb);
}

void DebugerOutputTab::endQt(std::shared_ptr<pBear::ConnectionClient::Process> pro, pBear::debuger::TerminalProcessConnectorDebuger* deb)
{
    for (int i = 0; i < ui->tabWidget->count(); i++)
    {
        if (dynamic_cast<DebugerOutput*>(ui->tabWidget->widget(i))->getDeb() == deb)
        {
            DebugerOutput *wid = dynamic_cast<DebugerOutput*>(ui->tabWidget->widget(i));
            wid->end(pro);
            ui->tabWidget->removeTab(i);
            delete wid;
            return;
        }
    }
}

extern "C" {
    QWidget *getWidget(std::shared_ptr<pBear::corre::Event> event)
    {
        return new DebugerOutputTab;
    }
    std::string type()
    {
        return "QDockWidget";
    }
    std::string orientation()
    {
        return "right";
    }
    QMenu* getMenu(std::shared_ptr<pBear::corre::Event> event)
    {
        return new QMenu("Debuger Output");
    }
}

