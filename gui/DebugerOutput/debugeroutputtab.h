#ifndef DEBUGEROUTPUTTAB_H
#define DEBUGEROUTPUTTAB_H

#include <QWidget>

#include "terminalprocessconnectordebuger.h"

namespace Ui {
class DebugerOutputTab;
}

Q_DECLARE_METATYPE(std::shared_ptr<pBear::ConnectionClient::Process>)
Q_DECLARE_METATYPE(pBear::debuger::TerminalProcessConnectorDebuger*)


class DebugerOutputTab : public QWidget
{
    Q_OBJECT

public:
    explicit DebugerOutputTab(QWidget *parent = 0);
    ~DebugerOutputTab();

    void start(std::shared_ptr<pBear::ConnectionClient::Process> pro, pBear::debuger::TerminalProcessConnectorDebuger* deb);
    void end(std::shared_ptr<pBear::ConnectionClient::Process> pro, pBear::debuger::TerminalProcessConnectorDebuger* deb);
private slots:
    void startQt(std::shared_ptr<pBear::ConnectionClient::Process> pro, pBear::debuger::TerminalProcessConnectorDebuger* deb);
    void endQt(std::shared_ptr<pBear::ConnectionClient::Process>, pBear::debuger::TerminalProcessConnectorDebuger*deb);
signals:
    void startQSignal(std::shared_ptr<pBear::ConnectionClient::Process>, pBear::debuger::TerminalProcessConnectorDebuger*);
    void endQtSignal(std::shared_ptr<pBear::ConnectionClient::Process>, pBear::debuger::TerminalProcessConnectorDebuger*);
private:
    Ui::DebugerOutputTab *ui;
};

#endif // DEBUGEROUTPUTTAB_H
