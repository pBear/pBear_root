#include "event.h"
#include "executedebugcompileterminal.h"

#include "debugeroutput.h"
#include "debugeroutputtab.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    DebugerOutputTab w;
    w.show();

    pBear::edt::ExecuteDebugCompileTerminal *ter = pBear::edt::ExecuteDebugCompileTerminal::instance();
    std::shared_ptr<pBear::ConnectionClient::Process> process = std::make_shared<pBear::ConnectionClient::Process>("127.0.0.1", 10006);

    pBear::corre::Event event;
    pBear::corre::Project pro = event.getOpenDocuments()->newProject("/home/aron");
    pBear::corre::Document doc = pro.newDocument("/home/aron/main.cpp");
    sleep(1);
    pro.setLanguage("c++");
    pro.setComandStructure(pBear::corre::Language::instance()->getLanguage("c++")->getComandStructure()[0]);

    ter->debug(pro, process);

    return a.exec();
}
