#ifndef MODEL_H
#define MODEL_H

#include <QStandardItemModel>

class Model: public QStandardItemModel
{
public:
    Model();
    bool canFetchMore(const QModelIndex & parent)                const;
    bool hasChildren(const QModelIndex & parent = QModelIndex()) const;
};

#endif // MODEL_H
