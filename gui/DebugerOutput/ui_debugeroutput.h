/********************************************************************************
** Form generated from reading UI file 'debugeroutput.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DEBUGEROUTPUT_H
#define UI_DEBUGEROUTPUT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DebugerOutput
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QTreeWidget *treeWidget;

    void setupUi(QWidget *DebugerOutput)
    {
        if (DebugerOutput->objectName().isEmpty())
            DebugerOutput->setObjectName(QStringLiteral("DebugerOutput"));
        DebugerOutput->resize(400, 300);
        verticalLayout = new QVBoxLayout(DebugerOutput);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(DebugerOutput);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        treeWidget = new QTreeWidget(DebugerOutput);
        treeWidget->setObjectName(QStringLiteral("treeWidget"));

        verticalLayout->addWidget(treeWidget);


        retranslateUi(DebugerOutput);

        QMetaObject::connectSlotsByName(DebugerOutput);
    } // setupUi

    void retranslateUi(QWidget *DebugerOutput)
    {
        DebugerOutput->setWindowTitle(QApplication::translate("DebugerOutput", "DebugerOutput", 0));
        label->setText(QString());
        QTreeWidgetItem *___qtreewidgetitem = treeWidget->headerItem();
        ___qtreewidgetitem->setText(2, QApplication::translate("DebugerOutput", "type", 0));
        ___qtreewidgetitem->setText(1, QApplication::translate("DebugerOutput", "value", 0));
        ___qtreewidgetitem->setText(0, QApplication::translate("DebugerOutput", "name", 0));
    } // retranslateUi

};

namespace Ui {
    class DebugerOutput: public Ui_DebugerOutput {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEBUGEROUTPUT_H
