QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DebugerTerminal
#TEMPLATE = app
TEMPLATE      = lib

QMAKE_CXXFLAGS += -std=c++14 -Wl,-R/home/aron/pBear/gui/build-debugerTerminals

SOURCES += debugerterminals.cpp \
    debugerterminal.cpp
    # main.cpp \

FORMS += \
    debugerterminals.ui

HEADERS += \
    debugerterminals.h \
    debugerterminal.h


LIBS += -L../../libs -lpBear_corre -lboost_filesystem -lboost_system -lboost_regex -lpBear_executedebugcompileterminal

LIBS += -L/home/aron/pBear/libs   -lboost_system -lboost_thread -lpthread -lboost_serialization -lpBear_termical_process -lpBear_execute -lpBear_debuger
LIBS += -lpBear_compiler -lpBear_corre -lboost_regex -lpthread  -lboost_regex -lpBear_termical_process -lboost_system -lboost_thread -lpthread -lboost_serialization
LIBS += -L/usr/local/lib/ -lqtermwidget5

INCLUDEPATH += ../../corre/include ../../ExecuteDebugCompileTerminal ../../compiler ../../Execute ../../debuger ../../TerminalProcess/client/
DEPENDPATH  += ../../libs /usr/local/lib/
