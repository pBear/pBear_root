#include "debugerterminal.h"

#include "executedebugcompileterminal.h"
#include "analizerstreamgdb.h"

using namespace pBear;

DebugerTerminal::DebugerTerminal(std::shared_ptr<pBear::ConnectionClient::Process> pro, pBear::debuger::TerminalProcessConnectorDebuger *tcd)
{
    this->pro     = pro;
    process = std::make_shared<pBear::ConnectionClient::Process>("127.0.0.1", 10006);
    detTerInter = std::make_shared<DebugerTerminalInterface>(tcd);
    std::function<void (std::shared_ptr<ConnectionClient::ProcessQueneElement_Interface>, std::shared_ptr<ConnectionClient::Process>)> fun1, fun2;
    tcd->getControler()->getAnalizer<pBear::debuger::AnalizerStreamGDB>()->addNormalStreamFunction(std::bind(&DebugerTerminalInterface::resive, detTerInter.get(), std::placeholders::_1));

    pBear::edt::ExecuteDebugCompileTerminal::prepareGUI(process, {detTerInter},
                                                        std::bind(&DebugerTerminal::lanch, this, std::placeholders::_1),
                                                        fun1,
                                                        fun2);
}

void DebugerTerminal::lanch(std::string comand)
{
    sendText(QString::fromStdString(comand + "\n"));
}

std::shared_ptr<ConnectionClient::Process> DebugerTerminal::getProcess()
{
    return pro;
}


DebugerTerminalInterface::DebugerTerminalInterface(pBear::debuger::TerminalProcessConnectorDebuger *debugerInter)
{
    this->debugerInter = debugerInter;
    mu.lock();
}

void DebugerTerminalInterface::start(std::shared_ptr<ConnectionClient::Process> pro, std::shared_ptr<ConnectionClient::Connection> con)
{
    std::cout << "################################################################first" << std::endl;
    this->con = con;
    pBear::ConnectionClient::Process_server::remoteDisplay(false, con);
    pBear::ConnectionClient::Process_server::inputToOutput(true, con);
    pBear::ConnectionClient::Process_server::inputToInput(true, con);
    pBear::ConnectionClient::Process_server::makeProcess("/home/aron/pBear/gui/DebugerTerminals/process", con);
    mu.unlock();
}

void DebugerTerminalInterface::run(std::string data, ProcessData::Type proData)
{
    if (std::find(data.begin(), data.end(), '\n') != data.end()) block = true;
    debugerInter->send(data);
}

bool DebugerTerminalInterface::end()
{

}

void DebugerTerminalInterface::resive(std::string line)
{
    if (block)
    {
        block = false;
        return;
    }
    std::call_once(once, [this](){ mu.lock(); });
    pBear::ConnectionClient::Process_server::send(line, con);
    //std::cout << line << std::flush;
}
