#ifndef DEBUGERTERMINAL_H
#define DEBUGERTERMINAL_H

#include "qtermwidget5/qtermwidget.h"

#include "terminalprocessconnectordebuger.h"

#include <memory>
#include <mutex>

class DebugerTerminalInterface: public pBear::ConnectionClient::ProcessQueneElement_Interface
{
private:
    std::shared_ptr<pBear::ConnectionClient::Connection> con;
    std::mutex                                           mu;
    std::once_flag                                       once;
    pBear::debuger::TerminalProcessConnectorDebuger     *debugerInter;
    bool                                                 block = false;
public:
    DebugerTerminalInterface(pBear::debuger::TerminalProcessConnectorDebuger *debugerInter);
    void start(std::shared_ptr<pBear::ConnectionClient::Process> pro, std::shared_ptr<pBear::ConnectionClient::Connection> con);
    void run(std::string data, ProcessData::Type proData);
    bool end();
    void resive(std::string line);
};

class DebugerTerminal: public QTermWidget
{
private:
    std::shared_ptr<pBear::ConnectionClient::Process>    process;
    std::shared_ptr<pBear::ConnectionClient::Process>    pro;
    std::shared_ptr<DebugerTerminalInterface>            detTerInter;


public:
    explicit DebugerTerminal(std::shared_ptr<pBear::ConnectionClient::Process> pro, pBear::debuger::TerminalProcessConnectorDebuger *tcd);
    void lanch(std::string comand);
    std::shared_ptr<pBear::ConnectionClient::Process> getProcess();
};

#endif // DEBUGERTERMINAL_H
