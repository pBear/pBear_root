#ifndef DEBUGERTERMINALS_H
#define DEBUGERTERMINALS_H

#include "executedebugcompileterminal.h"

#include <memory>

#include <QWidget>

namespace Ui {
class DebugerTerminals;
}

Q_DECLARE_METATYPE(std::shared_ptr<pBear::ConnectionClient::Process>)
Q_DECLARE_METATYPE(pBear::debuger::TerminalProcessConnectorDebuger*)

namespace pBear
{
    class DebugerTerminals : public QWidget
    {
        Q_OBJECT

    public:
        explicit DebugerTerminals(QWidget *parent = 0);
        void addTerminal(std::shared_ptr<pBear::ConnectionClient::Process>,
                         pBear::debuger::TerminalProcessConnectorDebuger*);
        void closeTerminal(std::shared_ptr<pBear::ConnectionClient::Process> pro,
                           pBear::debuger::TerminalProcessConnectorDebuger*deb);

        ~DebugerTerminals();

    private slots:
        void addTerminalQT(std::shared_ptr<pBear::ConnectionClient::Process>,
                      pBear::debuger::TerminalProcessConnectorDebuger*);
        void closeTerminalQt(std::shared_ptr<pBear::ConnectionClient::Process>,
                                   pBear::debuger::TerminalProcessConnectorDebuger*);
    signals:
        void addTerminalQTSignal(std::shared_ptr<pBear::ConnectionClient::Process>,
                                 pBear::debuger::TerminalProcessConnectorDebuger*);
        void closeTerminalQtSignal(std::shared_ptr<pBear::ConnectionClient::Process>,
                                   pBear::debuger::TerminalProcessConnectorDebuger*);
    private:
        Ui::DebugerTerminals *ui;
    };
}

#endif // DEBUGERTERMINALS_H
