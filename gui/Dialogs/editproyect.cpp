#include "ui_editproyect.h"
#include "editproyect.h"
#include "event.h"

#include <stdlib.h>

#include <QFileDialog>
#include <QPushButton>
#include <boost/regex.hpp>
#include <boost/filesystem.hpp>

using namespace pBear;

editProyect *editProyect::once = nullptr;

void editProyect::setComboCompileExecuteCompileMode()
{
    ui->comboBox->clear();
    if (ui->language->currentIndex() < 1 || ui->language->count() < 2) return;

    for (auto x: pBear::corre::Language::instance()->getLanguage(ui->language->currentText().toStdString())->getComandStructure())
    {
        ui->comboBox->addItem(x.name.c_str());
    }


    /*if (!pro.getLanguage().empty())
    {
        ui->comboBox->setCurrentText(pro.getComandStructure()->name.c_str());
    }*/
}

editProyect::editProyect(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::editProyect)
{
    ui->setupUi(this);

    connect(ui->search, SIGNAL(clicked()), this, SLOT(search()));
    connect(ui->language, SIGNAL(currentIndexChanged(int)), this, SLOT(currentIndexChanged(int)));
}

editProyect::~editProyect()
{
    delete ui;
}

editProyect *editProyect::instance()
{
    if (!once) once = new editProyect;
    return once;
}

void editProyect::show(corre::Event *event, std::string language, QWidget *widget)
{
    this->event = event;
    this->setParent(widget);

    ui->proName->show();
    ui->language->show();
    ui->buttonBox->button(QDialogButtonBox::Ok)->hide();


    ui->language->clear();

    ui->language->addItem("none");
    for (std::pair<std::string, corre::LanguageDatas*> x: *corre::Language::instance())
    {
        ui->language->addItem(x.first.c_str());
        if (x.first == language)
        {
            ui->language->setCurrentIndex(ui->language->count() - 1);
        }
    }

    setComboCompileExecuteCompileMode();


    connect(ui->proName, SIGNAL(textChanged(QString)), this, SLOT(changeProName(QString)));

    setModal(true);
    setWindowFlags(Qt::Dialog);
    QDialog::show();
}

void editProyect::search()
{
    ui->name->setText(QFileDialog::getExistingDirectory(this, "path", "."));
}

void editProyect::accept()
{
    //std::string path = ui->name->text().toStdString();

    //if (boost::regex_search(path, boost::regex("^\\s*/")))
    /*{
        std::string path_loc = path;
        if (path_loc.back() != '/')path_loc += "/";
        path_loc += ui->proName->text().toStdString();
        pro.setName(path_loc);
    }*/
    /*else
    {
        pro.setName("");
    }
    pro.setLanguage(ui->language->currentText().toStdString());
    auto data = pBear::corre::Language::instance()->getLanguage(pro.getLanguage())->getComandStructure();
    std::string name = ui->comboBox->currentText().toStdString();
    auto it = std::find_if(data.begin(), data.end(), [name](pBear::corre::LanguageDatas::ComandStructure comandStructure)
    {
           return comandStructure.name == name;
    });
    pro.setComandStructure(*it);
    event->executeFuncProUpdate(pro);
    pBear::corre::Language::instance()->getLanguage(pro.getLanguage())->addDocument(pro);
    QDialog::accept();*/

    std::string path = ui->name->text().toStdString();
    if (path.back() != '/') path += "/";
    path += ui->proName->text().toStdString();
    if (path.back() == '/') path = path.substr(0, path.size() - 1);

    boost::filesystem::create_directory(path);

    pBear::corre::Project pro = event->getOpenDocuments()->newProject(path, ui->language->currentText().toStdString(),
                                          ui->comboBox->currentText().toStdString());
    for (auto x: pro.getComandStructure()->files)
    {
        std::ofstream of(path + "/" + x.first);
        of << x.second;
        of.close();
    }
    system(("cd " + path + ";/home/aron/pBear/projectParser/cmake/bin/cmake .").c_str());
    for (auto x: pro.getComandStructure()->files)
    {
        pBear::corre::Document doc = pro.newDocument(path + "/" + x.first);
    }
    QDialog::accept();
}

void editProyect::currentIndexChanged(int index)
{
    switch (index)
    {
    case  0:
        ui->buttonBox->button(QDialogButtonBox::Ok)->hide();
        break;
    default:
        //pro.setLanguage(ui->language->currentText().toStdString());
        ui->buttonBox->button(QDialogButtonBox::Ok)->show();
    }
    //setComboCompileExecuteCompileMode();
}

void editProyect::currentIndexCompileExecuteCompileChanged(int index)
{

}

void editProyect::changeProName(QString str)
{
    std::string path = ui->name->text().toStdString();
    if (path.back() != '/')path += "/";
    path += ui->proName->text().toStdString();
    if (!boost::filesystem::exists(path))
    {
        ui->label->setText("Valid project path");
        ui->buttonBox->button(QDialogButtonBox::Ok)->show();
    }
    else
    {
        ui->label->setText("Invalid project path");
        ui->buttonBox->button(QDialogButtonBox::Ok)->hide();
    }
}
