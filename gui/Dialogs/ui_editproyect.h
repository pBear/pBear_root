/********************************************************************************
** Form generated from reading UI file 'editproyect.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDITPROYECT_H
#define UI_EDITPROYECT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_editProyect
{
public:
    QVBoxLayout *verticalLayout;
    QComboBox *language;
    QComboBox *comboBox;
    QLineEdit *proName;
    QHBoxLayout *horizontalLayout;
    QLineEdit *name;
    QToolButton *search;
    QLabel *label;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *editProyect)
    {
        if (editProyect->objectName().isEmpty())
            editProyect->setObjectName(QStringLiteral("editProyect"));
        editProyect->resize(400, 179);
        verticalLayout = new QVBoxLayout(editProyect);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        language = new QComboBox(editProyect);
        language->setObjectName(QStringLiteral("language"));

        verticalLayout->addWidget(language);

        comboBox = new QComboBox(editProyect);
        comboBox->setObjectName(QStringLiteral("comboBox"));

        verticalLayout->addWidget(comboBox);

        proName = new QLineEdit(editProyect);
        proName->setObjectName(QStringLiteral("proName"));

        verticalLayout->addWidget(proName);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        name = new QLineEdit(editProyect);
        name->setObjectName(QStringLiteral("name"));

        horizontalLayout->addWidget(name);

        search = new QToolButton(editProyect);
        search->setObjectName(QStringLiteral("search"));

        horizontalLayout->addWidget(search);


        verticalLayout->addLayout(horizontalLayout);

        label = new QLabel(editProyect);
        label->setObjectName(QStringLiteral("label"));
        label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label);

        buttonBox = new QDialogButtonBox(editProyect);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(editProyect);
        QObject::connect(buttonBox, SIGNAL(accepted()), editProyect, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), editProyect, SLOT(reject()));

        QMetaObject::connectSlotsByName(editProyect);
    } // setupUi

    void retranslateUi(QDialog *editProyect)
    {
        editProyect->setWindowTitle(QApplication::translate("editProyect", "Dialog", 0));
        language->clear();
        language->insertItems(0, QStringList()
         << QApplication::translate("editProyect", "none", 0)
         << QApplication::translate("editProyect", "c", 0)
         << QApplication::translate("editProyect", "c++", 0)
        );
        search->setText(QApplication::translate("editProyect", "...", 0));
        label->setText(QApplication::translate("editProyect", "Enter project name", 0));
    } // retranslateUi

};

namespace Ui {
    class editProyect: public Ui_editProyect {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDITPROYECT_H
