#ifndef CENTRALWIDGETINTERFACE_H
#define CENTRALWIDGETINTERFACE_H

#include <string>

#include <QtPlugin>

class CentralWidgetInterface
{
public:
    virtual std::string getName() = 0;
};

#define CentralWidgetInterface_iid
Q_DECLARE_INTERFACE(CentralWidgetInterface, "pBear.CentralWidgetInterface")

#endif // CENTRALWIDGETINTERFACE_H
