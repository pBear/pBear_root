#ifndef GENERALPLUGININTERFACE_H
#define GENERALPLUGININTERFACE_H

#include <string>

#include <QtPlugin>

class GeneralPluginInterface
{
public:
    virtual std::string getName() = 0;
};

Q_DECLARE_INTERFACE(GeneralPluginInterface, "pBear.GeneralPluginInterface")

#endif // GENERALPLUGININTERFACE_H
