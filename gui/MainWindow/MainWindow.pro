#-------------------------------------------------
#
# Project created by QtCreator 2015-11-12T22:27:29
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS += -std=c++14

TARGET = MainWindow
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    verticalbutton.cpp \
    verticalpushbutton.cpp \
    ../Dialogs/editproyect.cpp \
    ../Dialogs/choseprojectname.cpp

HEADERS  += mainwindow.h \
    CentralWidgetInterface.h \
    GeneralPluginInterface.h \
    verticalbutton.h \
    verticalpushbutton.h \
    ../Dialogs/editproyect.h \
    ../Dialogs/choseprojectname.h

FORMS    += mainwindow.ui \
    verticalpushbutton.ui \
    ../Dialogs/editproyect.ui \
    ../Dialogs/choseprojectname.ui

LIBS += -lboost_filesystem -lboost_system

LIBS += -L../../libs -lpBear_corre -lboost_filesystem -lboost_system -lboost_regex -lpBear_executedebugcompileterminal

INCLUDEPATH += ../../corre/include /usr/include ../../ExecuteDebugCompileTerminal ../../compiler ../../Execute ../../debuger /home/aron/pBear/TerminalProcess/client/
DEPENDPATH  += ../../libs

LIBS += -L/home/aron/pBear/libs   -lboost_system -lboost_thread -lpthread -lboost_serialization -lpBear_termical_process -lpBear_execute -lpBear_debuger -ldl
LIBS += -lpBear_compiler -lpBear_corre -lboost_regex -lpthread  -lboost_regex -lpBear_termical_process -lboost_system -lboost_thread -lpthread -lboost_serialization
