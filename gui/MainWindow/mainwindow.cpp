#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "executedebugcompileterminal.h"

#include <QPluginLoader>
#include <QDockWidget>
#include <QSettings>
#include <QToolBar>
#include <QFileDialog>

#include <boost/log/trivial.hpp>

#include <functional>

#include <dlfcn.h>


#include <editproyect.h>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    event = std::make_shared<pBear::corre::Event>();

    ui->actionUndefined_project->setData("");
    connect(ui->actionUndefined_project, &QAction::triggered, std::bind(&MainWindow::addProject, this, ui->actionUndefined_project));

    loadEditMenu();

    QToolBar *leftToolBar = new QToolBar("leftToolBar");
    leftToolBar->setObjectName("leftToolBar");
    addToolBar(Qt::LeftToolBarArea, leftToolBar);
    leftToolBar->setFixedWidth(20);
    leftToolBar->addWidget(&left);
    left.setHideClickEnable(true);

    QToolBar *rightToolBar = new QToolBar("rightToolBar");
    rightToolBar->setObjectName("rightToolBar");
    addToolBar(Qt::RightToolBarArea, rightToolBar);
    rightToolBar->setFixedWidth(20);
    rightToolBar->addWidget(&right);
    right.setHideClickEnable(true);
    right.setInvert(true);

    QToolBar *bottomToolBar = new QToolBar("bottomToolBar");
    bottomToolBar->setObjectName("bottomToolBar");
    bottom = std::make_unique<verticalButtonBox>(true);
    addToolBar(Qt::BottomToolBarArea, bottomToolBar);
    bottomToolBar->setFixedHeight(20);
    bottomToolBar->addWidget(bottom.get());
    bottom->setHideClickEnable(true);
    bottom->setInvert(true);

    QToolBar *topToolBar =  new QToolBar("3");
    topToolBar->setObjectName("topToolBar");
    addToolBar(Qt::TopToolBarArea, topToolBar);

    setDockOptions(QMainWindow::AnimatedDocks | QMainWindow::AllowTabbedDocks | QMainWindow::AllowNestedDocks);

    //setTabPosition(Qt::LeftDockWidgetArea, QTabWidget::West);

    loadPlugins();

    QSettings settings("pBear", "pBear");
    for (QVariant x: settings.value("QDockWidgets").toList())
    {
        QString name = x.toString();
        addPluginStr(name);
    }
    QString centralWidget = settings.value("centralWidget").toString();
    if (!centralWidget.isEmpty())
    {
        addPluginStr(centralWidget);
    }
    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("windowState").toByteArray());

    connect(&timer, SIGNAL(timeout()), this, SLOT(postConfigure()));
    timer.setSingleShot(true);
    timer.setInterval(1000);
    timer.start();
}

void MainWindow::loadPlugins()
{
    std::string plugin_path;

    #ifndef GUI_PLUGIN_PATH
    #ifndef ROOT_PROJECT
    #error ROOT_PROJECT or GUI_PLUGIN_PATH must be defined (compile with -DLANGUAGE_PATH=...)
    #endif
            plugin_path =  ROOT_PROJECT + std::string("/gui/plugins");
    #else
            languagePath = GUI_PLUGIN_PATH;
    #endif

    boost::filesystem::directory_iterator end;
    boost::filesystem::path path(plugin_path);


    for (boost::filesystem::directory_iterator iter(path); iter != end; iter++)
    {
        if (boost::filesystem::is_regular_file(iter->path()))
        {
            QAction * action = ui->menuPlugins->addAction(QString::fromStdString(iter->path().filename().string()));
            action->setCheckable(true);
            action->setChecked(false);
            action->setData(QString::fromStdString(iter->path().string()));
            connect(action, &QAction::triggered, std::bind(&MainWindow::addPlugin, this, action, std::placeholders::_1));
        }
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QSettings settings("pBear", "pBear");
    settings.setValue("geometry", saveGeometry());
    settings.setValue("windowState", saveState());
    settings.setValue("centralWidget", centralWidget()->objectName());
    QList<QVariant> list;
    for (QDockWidget *wid: findChildren<QDockWidget *>())
    {
        list.append(wid->objectName());
    }
    settings.setValue("QDockWidgets", list);
    for (auto fun: closeFuns)
    {
        fun();
    }
    QMainWindow::closeEvent(event);
}

void MainWindow::loadEditMenu()
{
    for (auto x: *pBear::corre::Language::instance())
    {
        QMenu *tmpLan = ui->menuNew_Project->addMenu(x.first.c_str());

        for (pBear::corre::LanguageDatas::ComandStructure y: x.second->getComandStructure())
        {
            QAction *lanMode = tmpLan->addAction(y.name.c_str());
            lanMode->setProperty("language", QString::fromStdString(x.first));
            lanMode->setProperty("ComandStructure", y.name.c_str());
            connect(lanMode, &QAction::triggered, std::bind(&MainWindow::addProject, this, lanMode));
        }
        QAction *lanMode = tmpLan->addAction("none");
        lanMode->setProperty("language", "");
        lanMode->setProperty("ComandStructure", "");
        connect(lanMode, &QAction::triggered, std::bind(&MainWindow::addProject, this, lanMode));
    }

    QMenu *tmp = ui->menuEdit->addMenu("open Project");
    for (auto x: *pBear::corre::Language::instance())
    {

        QMenu *tmpLan = tmp->addMenu(x.first.c_str());

        for (pBear::corre::LanguageDatas::ComandStructure y: x.second->getComandStructure())
        {
            QAction *lanMode = tmpLan->addAction(y.name.c_str());
            lanMode->setProperty("language", QString::fromStdString(x.first));
            lanMode->setProperty("ComandStructure", y.name.c_str());
            connect(lanMode, &QAction::triggered, std::bind(&MainWindow::openProject, this, lanMode));
        }
        QAction *lanMode = tmpLan->addAction("none");
        lanMode->setProperty("language", "");
        lanMode->setProperty("ComandStructure", "");
        connect(lanMode, &QAction::triggered, std::bind(&MainWindow::openProject, this, lanMode));
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addPluginStr(QString plugin)
{
    for (QAction *x: ui->menuPlugins->actions())
    {
        if (x->data().toString() == plugin)
        {
            x->setChecked(true);
            addPlugin(x, x->isChecked());
            return;
        }
    }
}

void MainWindow::addPlugin(QAction *action, bool state)
{
    QString plugin = action->data().toString().toStdString().c_str();
    auto it = qDockWidgets.find(plugin.toStdString());
    if (it != qDockWidgets.end() and state) return;
    if (it != qDockWidgets.end() and !state)
    {
        action->property("QDockWidget");
        QWidget *tmp = (QWidget*)(action->property("Plugin").value<void*>());
        tmp->disconnect();
        left.removeButton(tmp);
        right.removeButton(tmp);
        bottom->removeButton(tmp);
        top.removeButton(tmp);
        //removeDockWidget(tmp);
        //delete tmp->widget();
        delete tmp;
        qDockWidgets.erase(it);
        return;
    }
    qDockWidgets[action->data().toString().toStdString()] = action;

    if (!boost::filesystem::exists(plugin.toStdString())) return;

    void *hndl = dlopen(plugin.toStdString().c_str(), RTLD_NOW);
    if(hndl == NULL)
    {
       std::cerr << dlerror() << std::endl;
       exit(-1);
    }
    std::function<QWidget*(std::shared_ptr<pBear::corre::Event>)> getWidget = (QWidget*(*)(std::shared_ptr<pBear::corre::Event>)) dlsym(hndl, "getWidget");
    std::function<std::string()> type        = (std::string(*)()) dlsym(hndl, "type");
    std::function<std::string()> orientation = (std::string(*)()) dlsym(hndl, "orientation");
    std::function<QMenu*(std::shared_ptr<pBear::corre::Event>)>      getMenu = (QMenu*(*)(std::shared_ptr<pBear::corre::Event>)) dlsym(hndl, "getMenu");
    std::function<void()>        closeTmp = (void(*)()) dlsym(hndl, "close");
    if (closeTmp)
    {
        closeFuns.push_back(closeTmp);
    }

    QMenu *menu = getMenu(event);
    if (!menu->actions().empty())
    {
        menuBar()->addMenu(menu);
        addActions(menu->actions());
    }
    else
    {
        delete menu;
    }

    if (type() == "QDockWidget")
    {
        QWidget *widget = getWidget(event);
        boost::filesystem::path path(plugin.toStdString());
        QDockWidget *dockWidget = new QDockWidget(path.filename().c_str());
        dockWidget->setObjectName(QString::fromStdString(plugin.toStdString()));
        dockWidget->setWidget(widget);
        action->setProperty("Plugin", qVariantFromValue((void*)dockWidget));
        if (orientation() == "left")
        {
            left.addButton(path.filename().c_str(), dockWidget);
            connect(dockWidget, &QDockWidget::dockLocationChanged, std::bind(&MainWindow::changeQDockWidget, this, dockWidget, std::placeholders::_1));
            addDockWidget(Qt::LeftDockWidgetArea, dockWidget);
        }
        else if (orientation() == "right")
        {
            right.addButton(path.filename().c_str(), dockWidget);
            connect(dockWidget, &QDockWidget::dockLocationChanged, std::bind(&MainWindow::changeQDockWidget, this, dockWidget, std::placeholders::_1));
            addDockWidget(Qt::RightDockWidgetArea, dockWidget);
        }
        else if (orientation() == "bottom")
        {
            bottom->addButton(path.filename().c_str(), dockWidget);
            connect(dockWidget, &QDockWidget::dockLocationChanged, std::bind(&MainWindow::changeQDockWidget, this, dockWidget, std::placeholders::_1));
            addDockWidget(Qt::BottomDockWidgetArea, dockWidget);
        }
        else if(orientation() == "top")
        {
            top.addButton(path.filename().c_str(), dockWidget);
            connect(dockWidget, &QDockWidget::dockLocationChanged, std::bind(&MainWindow::changeQDockWidget, this, dockWidget, std::placeholders::_1));
            addDockWidget(Qt::TopDockWidgetArea, dockWidget);
        }

    }
    else if (type() == "CentralWidget")
    {
        QWidget *widget = getWidget(event);
        widget->setObjectName(plugin);
        setCentralWidget(widget);
        action->setProperty("Plugin", qVariantFromValue((void*)widget));
    }
}

void MainWindow::addProject(QAction *action)
{
    pBear::editProyect::instance()->show(event.get(), action->property("language").toString().toStdString(), this);
}

void MainWindow::openProject(QAction *action)
{
    QString extensions;
    for (pBear::corre::LanguageDatas::Item x: pBear::corre::Language::instance()->getLanguage(action->property("language").toString().toStdString())->getItems())
    {
        if (x.config)
        {
            for (std::string y: x.extencions)
            {
                extensions += (" *" + y).c_str();
            }
        }
    }
    QString file = QFileDialog::getOpenFileName(this,
        tr("Open "), QDir::homePath(), "Image Files (" + extensions + ")");

    if (file.isEmpty()) return;

    pBear::corre::Project pro;
    try
    {
        pro = event->getOpenDocuments()->newProject(QFileInfo(file).absoluteDir().path().toStdString(),
                                             action->property("language").toString().toStdString(),
                                             action->property("ComandStructure").toString().toStdString());
        pro.newDocument(file.toStdString());
    }
    catch (pBear::corre::ExceptionNotValidProjectPah &ex)
    {
        BOOST_LOG_TRIVIAL(warning) << ex.what();
    }
    catch (pBear::corre::ExceptionNotValidDocumentName &ex)
    {
        event->getOpenDocuments()->removePro(pro);
        BOOST_LOG_TRIVIAL(warning) << ex.what();
    }
}

void MainWindow::changeQDockWidget(QDockWidget *wid, Qt::DockWidgetArea area)
{
    if (left.hasButton(wid) and area == Qt::LeftDockWidgetArea) return;
    left.removeButton(wid);

    if (right.hasButton(wid) and area == Qt::RightDockWidgetArea) return;
    right.removeButton(wid);

    if (bottom->hasButton(wid) and area == Qt::BottomDockWidgetArea) return;
    bottom->removeButton(wid);

    if (top.hasButton(wid) and area == Qt::TopDockWidgetArea) return;
    top.removeButton(wid);

    boost::filesystem::path path(wid->objectName().toStdString());

    switch (area) {
    case Qt::LeftDockWidgetArea:
        left.addButton(path.filename().c_str(), wid);
        break;
    case Qt::RightDockWidgetArea:
        right.addButton(path.filename().c_str(), wid);
        break;
    case Qt::BottomDockWidgetArea:
        bottom->addButton(path.filename().c_str(), wid);
        break;
    default:
        break;
    }
    connect(wid, &QDockWidget::dockLocationChanged, std::bind(&MainWindow::changeQDockWidget, this, wid, std::placeholders::_1));
}

void MainWindow::postConfigure()
{
    left.postConigureHide();
    right.postConigureHide();
}



void MainWindow::on_actionExecute_triggered()
{
    if (!event->getOpenDocuments()->getCurrentPro())
    {
        return;
    }
    std::shared_ptr<pBear::ConnectionClient::Process> process =  pBear::ConnectionClient::Process_server::instance()->getProcessByCurrentConfiguration();
    pBear::edt::ExecuteDebugCompileTerminal::instance()->
            execute(event->getOpenDocuments()->getCurrentPro(),
                    process);
}

void MainWindow::on_actionBuild_triggered()
{
    if (!event->getOpenDocuments()->getCurrentPro())
    {
        return;
    }
    std::shared_ptr<pBear::ConnectionClient::Process> process =  pBear::ConnectionClient::Process_server::instance()->getProcessByCurrentConfiguration();
    pBear::edt::ExecuteDebugCompileTerminal::instance()->
            build(event->getOpenDocuments()->getCurrentPro(),
                    process);
}

void MainWindow::on_actionDebug_triggered()
{
    if (!event->getOpenDocuments()->getCurrentPro())
    {
        return;
    }
    std::shared_ptr<pBear::ConnectionClient::Process> process =  pBear::ConnectionClient::Process_server::instance()->getProcessByCurrentConfiguration();
    pBear::edt::ExecuteDebugCompileTerminal::instance()->
            debug(event->getOpenDocuments()->getCurrentPro(),
                    process);
}
