#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "CentralWidgetInterface.h"
#include "GeneralPluginInterface.h"

#include "verticalbutton.h"


#include "event.h"

#include <map>

#include <boost/filesystem.hpp>
#include <functional>

#include <QMainWindow>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    std::vector<std::function<void()>> closeFuns;
    std::shared_ptr<pBear::corre::Event> event;
    verticalButtonBox left;
    verticalButtonBox right;
    verticalButtonBox top;
    std::unique_ptr<verticalButtonBox> bottom;
    std::map<std::string, QAction*> qDockWidgets;
    QTimer timer;
public:
    explicit MainWindow(QWidget *parent = 0);
    void loadPlugins();
    void closeEvent(QCloseEvent *event);
    void loadEditMenu();
    ~MainWindow();
private slots:
    void addPluginStr(QString plugin);
    void addPlugin(QAction *action, bool state);
    void addProject(QAction *action);
    void openProject(QAction *action);
    void changeQDockWidget(QDockWidget*wid, Qt::DockWidgetArea area);
    void postConfigure();

    void on_actionExecute_triggered();

    void on_actionBuild_triggered();

    void on_actionDebug_triggered();

private:
    Ui::MainWindow *ui;
};


#endif // MAINWINDOW_H
