/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionUndefined_project;
    QAction *actionExecute;
    QAction *actionBuild;
    QAction *actionDebug;
    QAction *action;
    QWidget *centralWidget;
    QStatusBar *statusBar;
    QMenuBar *menuBar;
    QMenu *menuPlugins;
    QMenu *menuEdit;
    QMenu *menuNew_Project;
    QMenu *menuOpen_Project;
    QMenu *menuCommands;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(916, 401);
        actionUndefined_project = new QAction(MainWindow);
        actionUndefined_project->setObjectName(QStringLiteral("actionUndefined_project"));
        actionExecute = new QAction(MainWindow);
        actionExecute->setObjectName(QStringLiteral("actionExecute"));
        actionBuild = new QAction(MainWindow);
        actionBuild->setObjectName(QStringLiteral("actionBuild"));
        actionDebug = new QAction(MainWindow);
        actionDebug->setObjectName(QStringLiteral("actionDebug"));
        action = new QAction(MainWindow);
        action->setObjectName(QStringLiteral("action"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 916, 25));
        menuPlugins = new QMenu(menuBar);
        menuPlugins->setObjectName(QStringLiteral("menuPlugins"));
        menuEdit = new QMenu(menuBar);
        menuEdit->setObjectName(QStringLiteral("menuEdit"));
        menuNew_Project = new QMenu(menuEdit);
        menuNew_Project->setObjectName(QStringLiteral("menuNew_Project"));
        menuOpen_Project = new QMenu(menuEdit);
        menuOpen_Project->setObjectName(QStringLiteral("menuOpen_Project"));
        menuCommands = new QMenu(menuBar);
        menuCommands->setObjectName(QStringLiteral("menuCommands"));
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuEdit->menuAction());
        menuBar->addAction(menuCommands->menuAction());
        menuBar->addAction(menuPlugins->menuAction());
        menuEdit->addAction(menuNew_Project->menuAction());
        menuEdit->addAction(menuOpen_Project->menuAction());
        menuNew_Project->addAction(actionUndefined_project);
        menuOpen_Project->addAction(action);
        menuCommands->addAction(actionExecute);
        menuCommands->addAction(actionDebug);
        menuCommands->addAction(actionBuild);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionUndefined_project->setText(QApplication::translate("MainWindow", "undefined project", 0));
        actionExecute->setText(QApplication::translate("MainWindow", "Execute", 0));
        actionBuild->setText(QApplication::translate("MainWindow", "Build", 0));
        actionDebug->setText(QApplication::translate("MainWindow", "Debug", 0));
        action->setText(QString());
        menuPlugins->setTitle(QApplication::translate("MainWindow", "Plugins", 0));
        menuEdit->setTitle(QApplication::translate("MainWindow", "Edit", 0));
        menuNew_Project->setTitle(QApplication::translate("MainWindow", "new Project", 0));
        menuOpen_Project->setTitle(QApplication::translate("MainWindow", "openProject", 0));
        menuCommands->setTitle(QApplication::translate("MainWindow", "Commands", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
