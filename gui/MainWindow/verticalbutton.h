#ifndef VERTICALBUTTON_H
#define VERTICALBUTTON_H

#include "verticalpushbutton.h"

#include <QWidget>
#include <QVBoxLayout>
#include <QDockWidget>

#include <vector>

class verticalButtonBox : public QWidget
{
    Q_OBJECT

private:
    QLayout                    *lay;
    std::vector<QWidget*>          wids;
    std::vector<OrientationButton*>          buts;
    bool                           enableHideOnClick;
    bool                           invert;
    bool                           horizontal;
public:
    explicit verticalButtonBox(bool horizontal = false, QWidget *parent = 0);
    void     addButton(QString name, QWidget *widget);
    void     removeButton(QWidget *widget);
    void     setHideClickEnable(bool stat);
    bool     hasButton(QWidget *wid);
    void     postConigureHide();
signals:

public slots:
    void released();
    void changeActiveWidget(unsigned j);
    void changeActiveButton(unsigned j);
    void hideAll();
    void setInvert(bool state);
    void hideQDockWidget(QDockWidget *widget, bool state);
    void topLevelChangedQDockWidget(QDockWidget *widget, bool topLevel);
};

#endif // VERTICALBUTTON_H
