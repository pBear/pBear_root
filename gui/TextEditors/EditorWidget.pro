#-------------------------------------------------
#
# Project created by QtCreator 2015-04-11T23:54:33
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets



QMAKE_CXXFLAGS += -std=c++14 -Wl,-R/home/aron/pBear/gui/TextEditors

LIBS += -pthread -lX11

TARGET = EditorWidget
#TEMPLATE = app
TEMPLATE      = lib

SOURCES += editorwidget.cpp \
    editor.cpp \
    line.cpp \
    autocompletmenu.cpp \
    texteditwidget.cpp \
    editorcontainer.cpp \
    editorplugin.cpp \
    plugincontainer.cpp \
    floatmessage.cpp
    #main.cpp \

HEADERS  += \
    editorwidget.h \
    editor.h \
    line.h \
    autocompletmenu.h \
    texteditwidget.h \
    editorcontainer.h \
    editorplugin.h \
    plugincontainer.h \
    floatmessage.h

FORMS    += \
    editorwidget.ui \
    editor.ui \
    editorcontainer.ui

LIBS += -L../../libs -lpBear_corre

INCLUDEPATH += ../../corre/include
DEPENDPATH += ../../libs

RESOURCES += \
    icons.qrc
