#ifndef AUTOCOMPLETEMENU_H
#define AUTOCOMPLETEMENU_H

#include "document.h"
#include "analizer.h"


#include <QMenu>
#include <QTextCursor>
#include <QTimer>
#include <QKeyEvent>
#include <QPlainTextEdit>
#include <QTimer>
#include <QListWidget>
#include <QGridLayout>
#include <QDesktopWidget>
#include <QTextCursor>
#include <QPlainTextEdit>

namespace pBear
{
    namespace gui
    {
        class AutocompleteMenu :public QListWidget
        {
            Q_OBJECT
        private:
            QTimer           time;
            QPlainTextEdit  *text;
            int              size;
            int              textSize;
            bool             hasInfoShow;
            bool             block;
            corre::Document  doc;
            QPlainTextEdit       *edit;

            std::vector<corre::compData> comps;

            void focusOutEvent(QFocusEvent *event);
            void insertText();
        public:
            explicit AutocompleteMenu(QPlainTextEdit * edit, QWidget *parent);
            void setDoc(corre::Document doc);
            void exec();
        public slots:
            void keyPressEvent(QKeyEvent*event);
            void keyReleaseEvent(QKeyEvent*event);
            void itemChanged(QListWidgetItem*, QListWidgetItem*);
            void mouseDoubleClickEvent(QMouseEvent*);
        signals:
            void keyPress(QKeyEvent *);
            void keyRell(QKeyEvent *);
        };
    }
}
#endif
