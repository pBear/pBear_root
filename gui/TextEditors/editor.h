#ifndef EDITOR_H
#define EDITOR_H

#include "document.h"

#include <atomic>

#include <QDialog>
#include <QPlainTextEdit>
#include <QSyntaxHighlighter>
#include <QTimer>

namespace Ui {
class Editor;
}

namespace pBear {
    namespace gui {
        class ColorizeText: public QSyntaxHighlighter
        {
            Q_OBJECT
        private:
            corre::Document doc;

            void setColor(int index, int length, std::string color);
            void highlightBlock(const QString &text);
        public:
            explicit ColorizeText(QTextDocument *parent, corre::Document doc);
        };

        class Editor : public QDialog
        {
            Q_OBJECT
        private:
            Ui::Editor                   *ui;
            corre::Document               doc;
            std::shared_ptr<ColorizeText> col;
            std::atomic<bool>             des;

            std::vector<unsigned>         lines;
            std::vector<unsigned>         top;
            std::vector<unsigned>         booton;

            QTimer                         time;

            void paintEvent(QPaintEvent *event);
        public:
            explicit Editor(corre::Document doc, QWidget *parent = 0);
            ~Editor();
            corre::Document &getDoc();
            QPlainTextEdit *getTextEdit();
            void             updateLanguage();
            void             repaintLocal();
            void             hideSeeBlock(corre::Document::iterator it, bool hide);
        private slots:
            void contentsChange(int position, int charsRemoved, int charsAdded);
            void execAutocompletMenu();
        public slots:
            void restartTimer();
            void upLineData(int = 0);
            void repaintPlugins();
        signals:
            void restartTimerSignal();
        };
    }
}

#endif // EDITOR_H
