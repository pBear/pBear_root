#ifndef EDITORCONTAINER_H
#define EDITORCONTAINER_H

#include "editorwidget.h"
#include "event.h"

#include <QWidget>
#include <event.h>

#include <QGridLayout>

#include <qsplitter.h>

#include <stack>

namespace Ui {
class EditorContainer;
}

namespace pBear
{
    namespace gui
    {
        class EditorContainer : public QWidget
        {
            Q_OBJECT
        private:
            std::shared_ptr<pBear::corre::Event> eve;
            QSplitter                           *splitter = nullptr;
            int                                  count = 1;
            EditorWidget                        *currentEditor = nullptr;

            void getPos(int &i, int &j, QGridLayout *layout, EditorWidget *editor);
            void getPos(int &i, int &j, QGridLayout *layout, QGridLayout *layout1);
            QGridLayout *getParentLayout(QGridLayout *currLayout, QGridLayout *layout1);

            void clearUnused(QSplitter *layout);
            void clearComprime(QSplitter *lay);

            std::pair<bool, EditorWidget*> getNextCurrentEditor(QSplitter *lay, EditorWidget* editor);
            void loadDoc(pBear::corre::Document doc);

        public:
            explicit EditorContainer(std::shared_ptr<pBear::corre::Event> eve, QWidget *parent = 0);
            void repaintLocal();
            ~EditorContainer();
        public slots:
            void slotRepaintPluginsUp();
        signals:
           void signalRepaintPluginsUp();
        private slots:
            void addHorizontal(EditorWidget *editor);
            void addVertical(EditorWidget *editor);
            void closeEditor(EditorWidget *editor);
            void currentEditorChange(EditorWidget *editor);
        private:
            Ui::EditorContainer *ui;
        };
    }
}

#endif // EDITORCONTAINER_H
