#include "floatmessage.h"
#include "texteditwidget.h"

#include <QEvent>
#include <iostream>

using namespace pBear::gui;

FloatMessage* FloatMessage::once = nullptr;


FloatMessage *FloatMessage::instance()
{
    if (!once) once = new FloatMessage;
    return once;
}

void FloatMessage::setOption(std::list<corre::DiagnosticData> options)
{
    clear();
    for (corre::DiagnosticData x: options)
    {
        QTreeWidgetItem *item = new QTreeWidgetItem;
        item->setText(0, x.text.c_str());
        addTopLevelItem(item);
        for (corre::fixeData y: x.fixes)
        {
            QTreeWidgetItem *fixItem = new QTreeWidgetItem;
            fixItem->setText(0, y.text.c_str());
            item->addChild(fixItem);
        }

    }
}

FloatMessage::FloatMessage(QWidget *parent) :
    QTreeWidget(parent)
{
    setWindowFlags( Qt::CustomizeWindowHint|Qt::Dialog);
}

void     FloatMessage::focusInEvent(QFocusEvent *event)
{
    QTreeWidget::focusInEvent(event);
}

void    FloatMessage::exec(QPoint point, TextEditWidget *editor)
{   
    this->editor = editor;

    setParent(editor);
    setWindowFlags(Qt::Dialog);
    move(point);

    show();
    setFocus();
}

void    FloatMessage::focusOutEvent(QFocusEvent *eve)
{
    hide();
    QTreeWidget::focusOutEvent(eve);
}

void  FloatMessage::hide()
{

    QTreeWidget::hide();
    clear();
}


void    FloatMessage::itemDoubleClicked (QListWidgetItem * it)
{
    Item        *itu   = static_cast<Item*>(it);
    /*QTextCursor cursor = static_cast<editText*>(data)->textCursor();
    cursor.movePosition(QTextCursor::Start);
    cursor.movePosition(QTextCursor::NextBlock, QTextCursor::MoveAnchor, itu->fix.line - 1);
    cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::MoveAnchor, itu->fix.colum - 1);
    cursor.insertText(itu->text());
    hide();*/
}
