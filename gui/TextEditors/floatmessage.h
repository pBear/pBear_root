#ifndef FLOATMESSAGE_H
#define FLOATMESSAGE_H

#include <diagnosticData.h>

#include <QDialog>
#include <QString>
#include <QLabel>
#include <QListWidget>
#include <QVBoxLayout>
#include <QTimer>
#include <QTreeWidget>

namespace pBear
{
    namespace gui
    {
        class TextEditWidget;
        class FloatMessage : public QTreeWidget
        {
            Q_OBJECT
        private:
            struct Item: public QListWidgetItem
            {
                pBear::corre::fixeData fix;
            };

            QTreeWidget     *tree;

            QWidget         *invoqueWidget;
            TextEditWidget  *editor = nullptr;
            static FloatMessage* once;
            //methods
            explicit         FloatMessage(QWidget *parent = 0);
        public:
            static FloatMessage* instance();
            void             setOption(std::list<corre::DiagnosticData> options);
            void             focusInEvent(QFocusEvent *);
            void             focusOutEvent(QFocusEvent *);
            void             exec(QPoint, TextEditWidget *editor);
            void             hide();
        private slots:
            void             itemDoubleClicked (QListWidgetItem * item);
        };
    }
}

#endif // FLOATMESSAGE_H
