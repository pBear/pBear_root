#include "plugincontainer.h"

#include "texteditwidget.h"
#include "line.h"
#include "analizer.h"
#include "floatmessage.h"

#include <QTextOption>
#include <QPainter>
#include <QFont>

#include <iostream>

#include <unistd.h>

using namespace pBear::gui;


void Line::setDoc(const corre::Document &value)
{
    doc = value;
}

void Line::setTextEditWidget(TextEditWidget *wid)
{
    textEditWidget = wid;
}

void Line::repaintPlugins()
{
    repaint(lines, top, booton);
}

void Line::paintPlugins(unsigned top, unsigned bottom, pBear::corre::Document::iterator it)
{
    QPainter    painter(this);
    QRect       rec;

    painter.setRenderHint(QPainter::Antialiasing, true);
    int begin = isVisibleErrorPresent? error_left: line_left;

    rec.setTop(top + 2);
    rec.setBottom(bottom - 2);

    for (EditorPlugin x: *PluginContainer::instance())
    {
        rec.setLeft(begin - x.weight() + 2);
        rec.setRight(begin);
        if (!x.hide(it->getDocument()))
        {
            begin -= x.weight() + 2;
            painter.drawImage(rec, QImage(x.icon(&(*it)).c_str()));
        }
    }
}

void Line::paintLine(unsigned top, unsigned botton, unsigned line)
{
    if (this->lLine == top)
    {
        return;
    }
    else
    {
        this->lLine = top;
    }

    QPainter    painter(this);
    QRect       rec;
    QTextOption textOption;
    QFont       font;

    textOption.setAlignment(Qt::AlignRight| Qt::AlignVCenter);

    rec.setLeft(line_left = (rect().right() - 8 - maxDigits*number_size));
    rec.setRight(rect().right() - 8);

    rec.setTop(top);
    rec.setBottom(botton);

    font.setPointSize(8);

    if (boldLine == line)
    {
        font.setBold(true);
    }
    else
    {
        font.setBold(false);
    }
    painter.setFont(font);
    painter.drawText(rec, std::to_string(line + 1).c_str(), textOption);
}

void Line::paintError(unsigned top, unsigned botton,
                      corre::Document::iterator it)
{
    QPainter    painter(this);
    QRect       rec;
    unsigned    bits = 0;

    rec.setTop(top);
    rec.setBottom(botton);
    rec.setLeft((error_left = (line_left - error_size + 6)));
    rec.setRight(line_left + 6);

    std::list<corre::DiagnosticData> datas = it->getDiagnostics();

    if (!datas.empty())
    {
        for (auto x: datas)
        {
            switch (x.grav)
            {
            case corre::gravity::FATAL:
                    bits |= 1;
                    break;
            case corre::gravity::ERROR:
            case corre::gravity::NOIMPLEMENT:
                    bits |= 2;
                    break;
            case corre::gravity::WARNING:
                    bits |= 4;
                    break;
            case corre::gravity::NOTE:
                    bits |= 8;
                    break;
            }
        }
        switch (bits)
        {
            case 1:
                    painter.drawImage(rec, QImage(":/new/error_fatal.png"));
                    break;
            case 2:
                    painter.drawImage(rec, QImage(":/new/error_normal.png"));
                    break;
            case 3:
                    painter.drawImage(rec, QImage(":/new/images/icons/error_normal_fatal.png"));
                    break;
            case 4:
                    painter.drawImage(rec, QImage(":/new/error_alert.png"));
                    break;
            case 5:
                    painter.drawImage(rec, QImage(":/new/error_normal_fatal.png"));
                    break;
            case 6:
                    painter.drawImage(rec, QImage(":/new/error_normal_alert.png"));
                    break;
            case 7:
                     painter.drawImage(rec, QImage(":/new/error_all.png"));
                     break;
            case 8:
                    painter.drawImage(rec, QImage(":/new/error_note.png"));
                    break;
        }
    }
}

void Line::paintCollapse(unsigned top, unsigned botton, pBear::corre::Document::iterator it)
{
    if (it->isCollapaseble())
    {
        QPainter    painter(this);
        QRect       rec;

        rec.setTop(top + 5);
        rec.setBottom(botton - 5);
        rec.setLeft(rect().right() -6);
        rec.setRight(rect().right() -0);
        if (it->getCollapsed())
        {
            painter.drawImage(rec, QImage(":/new/collapsed.png"));
        }
        else
        {
            painter.drawImage(rec, QImage(":/new/expanded.png"));
        }
    }
}

pBear::corre::Document::iterator Line::getBlockByPos(unsigned pos)
{
    for (unsigned i = 0; i < lines.size(); i++)
    {
        if (top[i] <= pos and booton[i] >= pos)
        {
            return doc.findByLine(lines[i]);
        }
    }
    return doc.end();
}

void Line::mousePressEvent(QMouseEvent *event)
{
    int cursorPosY = cursor().pos().y() - mapToGlobal(rect().topLeft()).y();
    int cursorPosX = cursor().pos().x() - mapToGlobal(rect().topLeft()).x();

    corre::Document::iterator it = getBlockByPos(cursorPosY);

    if (it == doc.end()) return;

    std::cout << rect().right() - collapse_size << std::endl;


    if (cursorPosX > rect().right() - collapse_size and cursorPosX <= rect().right())
    {
        if (it->isCollapaseble())
        {
            it->setCollapsed(!it->getCollapsed());
        }
    }


    if (line_left <= cursorPosX and cursorPosX < rect().right() - collapse_size)
    {
        for (EditorPlugin x: *PluginContainer::instance())
        {
            x.defaultAction(&(*it));
            break;
        }
    }
    else
    {
        int begin = isVisibleErrorPresent? error_left: line_left;
        for (EditorPlugin x: *PluginContainer::instance())
        {
            if (!x.hide(it->getDocument()))
            {
                if ((begin >= cursorPosX) and ((begin -x.weight() - 2) <= (int)cursorPosX))
                {
                    x.mouseClickEvent(&(*it));
                }
                begin -= x.weight() + 2;
            }
        }
    }

    QWidget::mousePressEvent(event);
    //QWidget::repaint();
}

void Line::mouseDoubleClickEvent(QMouseEvent *event)
{
    unsigned cursorPosY = cursor().pos().y() - mapToGlobal(rect().topLeft()).y();
    unsigned cursorPosX = cursor().pos().x() - mapToGlobal(rect().topLeft()).x();

    corre::Document::iterator it = getBlockByPos(cursorPosY);

    int begin = isVisibleErrorPresent? error_left: line_left;
    for (EditorPlugin x: *PluginContainer::instance())
    {
        if (!x.hide(it->getDocument()))
        {
            if (begin >= cursorPosX and (begin -x.weight() - 2) <= cursorPosX)
            {
                x.mouseDoubleClickEvent(&(*it));
            }
            begin -= x.weight() + 2;
        }
    }

    QWidget::mouseDoubleClickEvent(event);
}

void Line::mouseMoveEvent(QMouseEvent *event)
{
    unsigned cursorPosY = cursor().pos().y() - mapToGlobal(rect().topLeft()).y();
    unsigned cursorPosX = cursor().pos().x() - mapToGlobal(rect().topLeft()).x();

    corre::Document::iterator it = getBlockByPos(cursorPosY);

    int begin = isVisibleErrorPresent? error_left: line_left;
    for (EditorPlugin x: *PluginContainer::instance())
    {
        if (!x.hide(it->getDocument()))
        {
            if (begin >= cursorPosX and (begin -x.weight() - 2) <= cursorPosX)
            {
                x.mouseMoveEvent(&(*it));
            }
            begin -= x.weight() + 2;
        }
    }

    if (cursorPosX >= error_left && cursorPosX < line_left)
    {
        if (doc.end() != it && !it->getDiagnostics().empty())
        {
            FloatMessage::instance()->setOption(it->getDiagnostics());
            FloatMessage::instance()->exec(cursor().pos(), textEditWidget);
        }
    }

    QWidget::mouseMoveEvent(event);
}

void Line::updateWight()
{
    maxDigits = [&]()
    {
        int tmp = lines.back();
        int i = 0;
        for (; tmp >= 1; i++)
        {
            tmp /= 10;
        }
        return i;
    }();

    bool tmp = [&]()
    {
        corre::Document::iterator it  = doc.findByLine(lines.front());
        corre::Document::iterator end = doc.findByLine(lines.back());

        ++end;

        doc.getAnalizer()->blockThread();


        for (; it != end; ++it)
        {
            doc.getAnalizer()->unlockThread();
            if (!it->getDiagnostics().empty()) return true;
        }
        doc.getAnalizer()->unlockThread();
        return false;
    }();

    if (tmp)
    {
        isVisibleErrorPresent = true;
    }

    int pluginsSize = 0;
    for (EditorPlugin x: *PluginContainer::instance())
    {
        if (!x.hide(doc))
        {
            pluginsSize += x.weight() + 2;
        }
    }

    int size = (maxDigits*number_size + (isVisibleErrorPresent?error_size:0) + 5 + pluginsSize);

    setFixedWidth(size);

}

Line::Line(QWidget *parent) :
    QWidget(parent)
{
    setMouseTracking(true);
    setMinimumWidth(1);
}

Line::~Line()
{

}

void Line::paintEvent(QPaintEvent */*event*/)
{

    doc.getAnalizer()->blockThread();
    corre::Document::iterator it  = doc.findByLine(lines.front());
    corre::Document::iterator end = doc.findByLine(lines.back());

    QPainter painter(this);
    painter.eraseRect(rect());

    for (unsigned i = 0; i < lines.size(); ++i)
    {
        paintLine(top[i], booton[i], lines[i]);
        it = [](corre::Document::iterator it, corre::Document::iterator end, int line)
        {
            for (; it->getLine() != line and it != end; it++);
            {
                return it;
            }
            return end;
        }(it, end, lines[i]);
        if (isVisibleErrorPresent and it != doc.end())
        {
            paintError(top[i], booton[i], it);
        }
        if (it != doc.end())
        {
            paintCollapse(top[i], booton[i], it);
            paintPlugins(top[i], booton[i], it);
        }
    }
    lLine = 22222;
    doc.getAnalizer()->unlockThread();
}

void Line::repaint(std::vector<unsigned> lines, std::vector<unsigned> top, std::vector<unsigned> booton)
{
    this->lines  = lines;
    this->top    = top;
    this->booton = booton;

    updateWight();

    QWidget::repaint();

}
