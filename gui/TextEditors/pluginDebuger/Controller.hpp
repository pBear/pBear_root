#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#include <executedebugcompileterminal.h>
#include <Breakpoint.h>

class Controller
{
private:
    std::shared_ptr<pBear::corre::Event> event;
     pBear::debuger::TerminalProcessConnectorDebuger *deb = nullptr;
    std::function<void()> funRepaint;
    static Controller* once;
    Controller();
public:
    static Controller* instance();
    void init(std::shared_ptr<pBear::corre::Event> event);
    void start(std::shared_ptr<pBear::ConnectionClient::Process> pro, pBear::debuger::TerminalProcessConnectorDebuger *deb);
    void end(std::shared_ptr<pBear::ConnectionClient::Process> pro, pBear::debuger::TerminalProcessConnectorDebuger *deb);
    void updateBreapoins(std::list<pBear::debuger::Breakpoint> breaks);
    void setRepaintFunction(std::function<void()> funRepaint);
    void executeRepaint();
    void clickEvent(pBear::corre::Block* block);
    void defaultAction(pBear::corre::Block* block);
};

#endif // CONTROLLER_HPP
