TEMPLATE = lib
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++14 -Wl,-R/home/aron/pBear/gui/build-EditorWidget-Desktop-Debug

LIBS += -pthread -lX11


SOURCES += \
    Controller.cpp

LIBS += -L../../../libs -lpBear_corre -lboost_filesystem -lboost_system -lboost_regex -lpBear_executedebugcompileterminal

INCLUDEPATH += ../../../corre/include ../../../ExecuteDebugCompileTerminal ../../../compiler ../../../Execute ../../../debuger /home/aron/pBear/TerminalProcess/client/
DEPENDPATH  += ../../../libs

LIBS += -L/home/aron/pBear/libs   -lboost_system -lboost_thread -lpthread -lboost_serialization -lpBear_termical_process -lpBear_execute -lpBear_debuger
LIBS += -lpBear_compiler -lpBear_corre -lboost_regex -lpthread  -lboost_regex -lpBear_termical_process -lboost_system -lboost_thread -lpthread -lboost_serialization


HEADERS += \
    Controller.hpp
