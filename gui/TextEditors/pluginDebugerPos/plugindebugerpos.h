#ifndef PLUGINDEBUGERPOS_H
#define PLUGINDEBUGERPOS_H

#include <event.h>
#include <terminalprocessconnectordebuger.h>

std::shared_ptr<pBear::corre::Event>              event;
std::function<void()>                             funRepaint;
pBear::debuger::TerminalProcessConnectorDebuger  *deb = nullptr;
std::shared_ptr<pBear::ConnectionClient::Process> pro;

void start(std::shared_ptr<pBear::ConnectionClient::Process> pro, pBear::debuger::TerminalProcessConnectorDebuger *deb);
void end(std::shared_ptr<pBear::ConnectionClient::Process> pro, pBear::debuger::TerminalProcessConnectorDebuger *deb);


#endif // PLUGINDEBUGERPOS_H
