#include "plugincontainer.h"

#include "functional"

#include <boost/filesystem.hpp>

#include <QMenu>
#include <QSettings>
#include <dlfcn.h>

extern QMenu *menu;
extern QSettings settings;

PluginContainer *PluginContainer::once = nullptr;

PluginContainer::PluginContainer(QObject *parent) :
    QObject(parent)
{
    connect(&timer, SIGNAL(timeout()), this, SLOT(loadQt()));
    timer.setSingleShot(true);
    timer.setInterval(10);
#ifndef EDITOR_PLUGIN_PATH
#ifndef ROOT_PROJECT
#error ROOT_PROJECT or ROOT_PROJECT must be defined (compile with -DLANGUAGE_PATH=...)
#endif
        folder =  ROOT_PROJECT + std::string("/gui/TextEditors/Plugins");
#else
        folder = EDITOR_PLUGIN_PATH;
#endif
}

PluginContainer::~PluginContainer()
{

}

PluginContainer *PluginContainer::instance()
{
    if (!once) once = new PluginContainer;
    return once;
}

void PluginContainer::init(std::shared_ptr<pBear::corre::Event> event, std::function<void()> repaint)
{
    this->event = event;
    this->repaint = repaint;
}

PluginContainer::iterator PluginContainer::begin()
{
    return plguinUsable.begin();
}

PluginContainer::iterator PluginContainer::end()
{
    return plguinUsable.end();
}

void PluginContainer::load()
{
    timer.start();
}

void PluginContainer::close()
{
    QList<QVariant> listNamesPlugins;
    for (QAction *action: plugins)
    {
        listNamesPlugins.push_back(action->property("name"));
    }
    settings.setValue("plugins2", listNamesPlugins);
}

void PluginContainer::triggeredQt(QAction *action, bool state)
{
    if (!state)
    {
        plugins.erase(plugins.find(action));
        plguinUsable.erase(std::find_if(plguinUsable.begin(), plguinUsable.end(), [action](EditorPlugin plugin)
        {
            return plugin.name == action->property("name").toString().toStdString();
        }));
    }
    else
    {
        plugins.insert(action);
        EditorPlugin plugin;
        void *hndl = dlopen(action->property("name").toString().toStdString().c_str(), RTLD_NOW);
        if(hndl == NULL)
        {
           std::cerr << dlerror() << std::endl;
           exit(-1);
        }

        plugin.name = action->property("name").toString().toStdString();
        plugin.hide = (bool(*)(pBear::corre::Document)) dlsym(hndl, "hide");
        plugin.icon = (std::string(*)(pBear::corre::Block*)) dlsym(hndl, "icon");
        plugin.mouseClickEvent       = (void(*)(pBear::corre::Block*)) dlsym(hndl, "mouseClickEvent");
        plugin.mouseDoubleClickEvent = (void(*)(pBear::corre::Block*)) dlsym(hndl, "mouseDoubleClickEvent");
        plugin.mouseMoveEvent        = (void(*)(pBear::corre::Block*)) dlsym(hndl, "mouseMoveEvent");
        plugin.weight                = (int(*)()) dlsym(hndl, "weight");
        plugin.defaultAction         = (void(*)(pBear::corre::Block*)) dlsym(hndl, "defaultAction");

        std::function<void(std::shared_ptr<pBear::corre::Event>)> start = (void(*)(std::shared_ptr<pBear::corre::Event>)) dlsym(hndl, "init");
        std::function<void(std::function<void()>)> setRepaintFunction   = (void(*)(std::function<void()>)) dlsym(hndl, "setRepaintFunction");

        setRepaintFunction(repaint);
        start(event);

        std::cout << plugin.hide(pBear::corre::Document()) << std::endl;

        plguinUsable.insert(plugin);
    }
}

void PluginContainer::loadQt()
{
    boost::filesystem::directory_iterator end;
    boost::filesystem::path path(folder);
    std::list<QAction*> listTmp;
    for (boost::filesystem::directory_iterator iter(path); iter != end; iter++)
    {
        if (boost::filesystem::is_regular_file(iter->path()))
        {
            QAction *action = menu->addAction(iter->path().filename().c_str());
            action->setCheckable(true);
            action->setProperty("name", iter->path().string().c_str());
            connect(action, &QAction::triggered, std::bind(&PluginContainer::triggeredQt, this, action, std::placeholders::_1));
            listTmp.push_back(action);
        }
    }
    for (QVariant x: settings.value("plugins2").toList())
    {
        for (QAction* action: listTmp)
        {
            QString name = x.toString();
            std::cout << name.toStdString() << " " << action->property("name").toString().toStdString() << std::endl;
            if (name == action->property("name").toString())
            {
                action->trigger();
            }
        }
    }
}
