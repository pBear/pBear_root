#ifndef PLUGINCONTAINER_H
#define PLUGINCONTAINER_H

#include "editorplugin.h"

#include <event.h>

#include <string>
#include <set>

#include <QObject>
#include <QAction>
#include <QSettings>
#include <QTimer>

class PluginContainer : public QObject
{
    Q_OBJECT
private:
    std::set<QAction*>      plugins;
    std::set<EditorPlugin>  plguinUsable;
    std::string             folder;// = "/home/aron/pBear/gui/TextEditors/Plugins";
    std::shared_ptr<pBear::corre::Event>  event;
    std::function<void()> repaint;
    static PluginContainer* once;
    QTimer                  timer;
public:
    using iterator = std::set<EditorPlugin>::iterator;

    explicit PluginContainer(QObject *parent = 0);
    ~PluginContainer();
    static PluginContainer* instance();
    void   init(std::shared_ptr<pBear::corre::Event> event, std::function<void()> repaint);

    iterator begin();
    iterator end();
    void load();
    void close();
signals:
    void loadEvent();
public slots:

private slots:
    void triggeredQt(QAction *action, bool state);
    void loadQt();
};

#endif // PLUGINCONTAINER_H
