#ifndef TEXTEDIT_H
#define TEXTEDIT_H

#include "document.h"

#include <QWidget>

#include <QMenu>
#include <QTextCursor>
#include <QTimer>
#include <QKeyEvent>
#include <QTextEdit>
#include <QTimer>
#include <QListWidget>
#include <QGridLayout>
#include <QDesktopWidget>
#include <QTextCursor>
#include <QPlainTextEdit>

namespace pBear
{
    namespace gui
    {
        class AutocompleteMenu1 :public QListWidget
        {
            Q_OBJECT
        private:
            QTimer           time;
            QPlainTextEdit  *text;
            int              size;
            int              textSize;
            bool             hasInfoShow;
            bool             block;
            corre::Document  doc;
            QTextEdit       *edit;
        public:
            explicit AutocompleteMenu1(corre::Document, QTextEdit * edit, QWidget *parent);
            void exec();
        /*public slots:
            void keyPressEvent(QKeyEvent*);
            void keyReleaseEvent(QKeyEvent*);
            void itemChanged(QListWidgetItem*, QListWidgetItem*);
            void necesaryHide();
            void mouseDoubleClickEvent(QMouseEvent*);
        signals:
            void keyPress(QKeyEvent *);
            void keyRell(QKeyEvent *);*/
        };
    }
}

#endif // TEXTEDIT_H
