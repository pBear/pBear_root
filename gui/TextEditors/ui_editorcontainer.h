/********************************************************************************
** Form generated from reading UI file 'editorcontainer.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDITORCONTAINER_H
#define UI_EDITORCONTAINER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_EditorContainer
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;

    void setupUi(QWidget *EditorContainer)
    {
        if (EditorContainer->objectName().isEmpty())
            EditorContainer->setObjectName(QStringLiteral("EditorContainer"));
        EditorContainer->resize(400, 300);
        verticalLayout = new QVBoxLayout(EditorContainer);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));

        verticalLayout->addLayout(gridLayout);


        retranslateUi(EditorContainer);

        QMetaObject::connectSlotsByName(EditorContainer);
    } // setupUi

    void retranslateUi(QWidget *EditorContainer)
    {
        EditorContainer->setWindowTitle(QApplication::translate("EditorContainer", "Form", 0));
    } // retranslateUi

};

namespace Ui {
    class EditorContainer: public Ui_EditorContainer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDITORCONTAINER_H
