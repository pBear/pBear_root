/********************************************************************************
** Form generated from reading UI file 'editorwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDITORWIDGET_H
#define UI_EDITORWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_EditorWidget
{
public:
    QVBoxLayout *verticalLayout;
    QTabWidget *TabEditors;
    QWidget *widget;
    QHBoxLayout *horizontalLayout_2;
    QToolButton *tool;
    QComboBox *addHorVer;
    QToolButton *close;
    QSpacerItem *horizontalSpacer;
    QComboBox *Language;
    QComboBox *TabSpace;
    QLabel *Info;

    void setupUi(QDialog *EditorWidget)
    {
        if (EditorWidget->objectName().isEmpty())
            EditorWidget->setObjectName(QStringLiteral("EditorWidget"));
        EditorWidget->resize(574, 330);
        verticalLayout = new QVBoxLayout(EditorWidget);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        TabEditors = new QTabWidget(EditorWidget);
        TabEditors->setObjectName(QStringLiteral("TabEditors"));
        TabEditors->setMinimumSize(QSize(0, 30));
        TabEditors->setStyleSheet(QLatin1String("QWidget:focus {\n"
"    border: 3px solid green;\n"
"   }"));

        verticalLayout->addWidget(TabEditors);

        widget = new QWidget(EditorWidget);
        widget->setObjectName(QStringLiteral("widget"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy);
        widget->setMinimumSize(QSize(0, 23));
        widget->setMaximumSize(QSize(16777215, 23));
        horizontalLayout_2 = new QHBoxLayout(widget);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        tool = new QToolButton(widget);
        tool->setObjectName(QStringLiteral("tool"));

        horizontalLayout_2->addWidget(tool);

        addHorVer = new QComboBox(widget);
        QIcon icon;
        icon.addFile(QStringLiteral(":/new/horIcon"), QSize(), QIcon::Normal, QIcon::Off);
        addHorVer->addItem(icon, QString());
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/new/verIcon"), QSize(), QIcon::Normal, QIcon::Off);
        addHorVer->addItem(icon1, QString());
        addHorVer->setObjectName(QStringLiteral("addHorVer"));
        addHorVer->setMaximumSize(QSize(44, 16777215));

        horizontalLayout_2->addWidget(addHorVer);

        close = new QToolButton(widget);
        close->setObjectName(QStringLiteral("close"));

        horizontalLayout_2->addWidget(close);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        Language = new QComboBox(widget);
        Language->setObjectName(QStringLiteral("Language"));
        Language->setMinimumSize(QSize(89, 0));
        Language->setMaximumSize(QSize(90, 16777215));

        horizontalLayout_2->addWidget(Language);

        TabSpace = new QComboBox(widget);
        TabSpace->setObjectName(QStringLiteral("TabSpace"));
        TabSpace->setMinimumSize(QSize(115, 0));
        TabSpace->setMaximumSize(QSize(115, 16777215));

        horizontalLayout_2->addWidget(TabSpace);

        Info = new QLabel(widget);
        Info->setObjectName(QStringLiteral("Info"));
        Info->setMinimumSize(QSize(165, 0));
        Info->setMaximumSize(QSize(165, 16777215));
        Info->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(Info);


        verticalLayout->addWidget(widget);


        retranslateUi(EditorWidget);

        TabEditors->setCurrentIndex(-1);
        TabSpace->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(EditorWidget);
    } // setupUi

    void retranslateUi(QDialog *EditorWidget)
    {
        EditorWidget->setWindowTitle(QApplication::translate("EditorWidget", "Dialog", 0));
        tool->setText(QApplication::translate("EditorWidget", "...", 0));
        addHorVer->setItemText(0, QString());
        addHorVer->setItemText(1, QString());

        close->setText(QApplication::translate("EditorWidget", "X", 0));
        TabSpace->clear();
        TabSpace->insertItems(0, QStringList()
         << QApplication::translate("EditorWidget", "Tab Width 2", 0)
         << QApplication::translate("EditorWidget", "Tab Width 4", 0)
         << QApplication::translate("EditorWidget", "Tab Width 8", 0)
         << QApplication::translate("EditorWidget", "Tab Width 16", 0)
         << QApplication::translate("EditorWidget", "Tab Width 32", 0)
         << QApplication::translate("EditorWidget", "Tab Width 64", 0)
        );
        Info->setText(QApplication::translate("EditorWidget", "TextLabel", 0));
    } // retranslateUi

};

namespace Ui {
    class EditorWidget: public Ui_EditorWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDITORWIDGET_H
