#include "baseproyectview.h"


#include "newclass.h"
#include "editproyect.h"
#include "newfile.h"
#include "openfiles.h"
#include "executedebugcompileterminal.h"

#include <boost/filesystem.hpp>

#include <iostream>
#include <QEvent>
#include <QMouseEvent><
#include <QMenu>
#include <QTextDocument>

using namespace pBear;

baseProyectView::baseProyectView(int id, corre::Event *event, QWidget *parent) :
    QTreeWidget(parent),
    menuProy(this),
    menuFile(this),
    id(id),
    event(event)
{
    //the menu for the proyect
    menuProy.addAction("execute");
    menuProy.addAction("build");
    menuProy.addAction("close");
    menuProy.addAction("edit");
    menuProy.addAction("save");

    //the sub menu for the proyect (add)
    menuProy.addMenu(&menuProyAdd);
    menuProyAdd.setTitle("add");
    menuProyAdd.addAction("class");
    menuProyAdd.addAction("header file");
    menuProyAdd.addAction("source file");
    menuProyAdd.addAction("CMake file");

    //the sub menu for the proyect (open)
    menuProy.addMenu(&menuProyOpen);
    menuProyOpen.setTitle("open");
    menuProyOpen.addAction("source files");
    menuProyOpen.addAction("header files");
    menuProyOpen.addAction("CMake file");
    menuProyOpen.addAction("open folder");

    //the file menu
    menuFile.addAction("save");
    menuFile.addAction("open");
    menuFile.addAction("close");
    menuFile.addAction("rename");
    menuFile.addAction("remove");

    //connect to slot
    connect(this, SIGNAL(itemPressed(QTreeWidgetItem*,int)), this, SLOT(itemClick(QTreeWidgetItem*,int)));
    connect(&menuProy, SIGNAL(triggered(QAction*)), this, SLOT(doMenuProyect(QAction*)));
    connect(&menuProyAdd, SIGNAL(triggered(QAction*)), this, SLOT(doMenuAddFile(QAction*)));
    connect(&menuProyOpen, SIGNAL(triggered(QAction*)), this, SLOT(doMenuOpenFiles(QAction*)));
    connect(&menuFile, SIGNAL(triggered(QAction*)), this, SLOT(doMenuFile(QAction*)));
    connect(this, SIGNAL(itemChanged(QTreeWidgetItem*,int)), this, SLOT(renameItem(QTreeWidgetItem*,int)));
    connect(this, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)), this, SLOT(itemDoubleClicked(QTreeWidgetItem*,int)));

    //iniviate value
    menuActive = false;

    //set mouse tracking
    setMouseTracking(true);

    //connect function to event manager
    void (baseProyectView::*tmp_1)(corre::Project)  = &baseProyectView::addItem;
    event->addFunNewProyect(std::bind(tmp_1, this, std::placeholders::_1));
    corre::Event::docData (baseProyectView::*tmp_2)(corre::Document) = &baseProyectView::addItem;
    event->addFunNewDocumentGUI(std::bind(tmp_2, this, std::placeholders::_1));
    event->addFuncDocLoad(std::bind(&baseProyectView::loadDoc, this, std::placeholders::_1));
    event->addFuncDocClose(std::bind(&baseProyectView::closeDoc, this, std::placeholders::_1));
    event->addFuncDocUnload(std::bind(&baseProyectView::unloadDoc, this, std::placeholders::_1));
    event->addFuncChangeLanguage(std::bind(&baseProyectView::changeLangnuage, this, std::placeholders::_1));
    event->addFuncUpdateDoc(std::bind(&baseProyectView::updateDocument, this, std::placeholders::_1));
}


void    baseProyectView::mouseDoubleClickEvent(QMouseEvent *event)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    if (itemClicked)
    {
        itemDoubleClicked(itemClicked, id);
    }
}

void baseProyectView::prepareMenusForProject(corre::Project pro)
{
    menuProyOpen.clear();
    menuProyAdd.clear();
    for (corre::LanguageDatas::Item item: pro.getItems())
    {
        if (item.showOpenMenu)
        {
            if (!item.name.empty())
            {
                menuProyOpen.addAction(item.name.c_str());
            }
            else
            {
                menuProyOpen.addAction("file");
            }
        }
        if (item.showAddMenu)
        {
            if (!item.name.empty())
            {
                menuProyAdd.addAction(item.name.c_str());
            }
            else
            {+

                menuProyAdd.addAction("file");
            }
        }
    }
    menuProyOpen.addAction("Open folder");
}

void baseProyectView::prepareMenusForFile(corre::Document doc)
{
    menuFile.clear();

    menuFile.addAction("save");
    menuFile.addAction("open");
    menuFile.addAction("close");
    menuFile.addAction("rename");
    menuFile.addAction("remove");

    switch (doc.getType())
    {
    case corre::Document::Type::CONFIG:
        menuFile.addAction("build");
        menuFile.addAction("execute");
        break;
    }
}

//doMenuAction

void baseProyectView::doMenuFile(QAction *option)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    //error managment
    _pbExeption.addFunction("tree::doMenuFile");
    if (!option)
    {
        _pbExeption << "doMenuFile";
        _pbExeption << "tree";
        _pbExeption << "the parameter QAction *option = " << option;
    }

    //actions
    if (option->text() == "save")
    {
        event->getOpenDocuments()->traductDoc(itemClicked).save();
        return;
    }
    if (option->text() == "open")
    {
        event->executeFuncDocLoad(event->getOpenDocuments()->traductDoc(itemClicked));
    }
    if (option->text() == "close")
    {
        event->getOpenDocuments()->traductDoc(itemClicked).close();
        return;
    }
    if (option->text() == "remove")
    {
        event->getOpenDocuments()->traductDoc(itemClicked).remove();
        delete itemClicked;
        return;
    }
    if (option->text() == "rename")
    {
        emit editItem(itemClicked, 0);
        return;
    }
}

void baseProyectView::doMenuProyect(QAction *option)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);

    //the error managment
    _pbExeption.addFunction("tree::doMenuProyect");
    if (!option)
    {
        _pbExeption << "addFunction";
        _pbExeption << "tree";
        _pbExeption << "the parameter option is = " << option;
        throw _pbExeption;
    }
    //actions
    if (option->text() == "execute")
    {
        std::shared_ptr<pBear::ConnectionClient::Process> process =  pBear::ConnectionClient::Process_server::instance()->getProcessByCurrentConfiguration();
        pBear::edt::ExecuteDebugCompileTerminal::instance()->
                execute(event->getOpenDocuments()->traductPro(itemClicked),
                        process);
        /*std::shared_ptr<pBear::ConnectionClient::Process> process1 =  pBear::ConnectionClient::Process_server::instance()->getProcessByCurrentConfiguration();
        pBear::edt::ExecuteDebugCompileTerminal::instance()->
                execute(event->getOpenDocuments()->traductPro(itemClicked),
                        process1);*/
        //sleep(100);

    }
    if (option->text() == "build")
    {
        std::shared_ptr<pBear::ConnectionClient::Process> process =  pBear::ConnectionClient::Process_server::instance()->getProcessByCurrentConfiguration();
        pBear::edt::ExecuteDebugCompileTerminal::instance()->
                build(event->getOpenDocuments()->traductPro(itemClicked),
                        process);
    }
    if (option->text() == "save")
    {
        event->getOpenDocuments()->traductPro(itemClicked).saveDocs();
        return;
    }
    if (option->text() == "close")
    {
        event->getOpenDocuments()->closeProyect(event->getOpenDocuments()->traductPro(itemClicked));
        delete itemClicked;
        return;
    }
    if (option->text() == "remove")
    {
        event->getOpenDocuments()->removePro(event->getOpenDocuments()->traductPro(itemClicked));
        delete itemClicked;
        return;
    }
    if (option->text() == "edit")
    {
        editProyect::instance()->show(event, event->getOpenDocuments()->traductPro(itemClicked), this);
        return;
    }
}

void baseProyectView::doMenuAddFile(QAction *option)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    //the error managment
    _pbExeption.addFunction("tree::doMenuAddFile");
    if (!option)
    {
        _pbExeption << "doMenuAddFile";
        _pbExeption << "tree";
        _pbExeption << "the parameter QAction *option = " << option;
        throw _pbExeption;
    }
    QString name = event->getOpenDocuments()->traductPro(itemClicked).getName().c_str();

    /*//the actions
    if (option->text() == "class")
    {
        //_newClass->show(event->getOpenDocuments()->traductPro(itemClicked));
    }
    if (option->text() == "source file")
    {
        //_newFile->show("source file", "cpp", name, "source", event->getOpenDocuments()->traductPro(itemClicked));
        return;
    }
    if (option->text() == "header file")
    {
        //_newFile->show("header file", "hpp", name, "header", event->getOpenDocuments()->traductPro(itemClicked));
        return;
    }
    if (option->text() == "CMake file")
    {
        // _configFile->show(name, configFile::type::CMAKE, true, itemClicked);
        return;
    }*/

   /* if (option->text() == "Open folder")
    {
        return;
    }
    else
    {*/
        pBear::corre::Project pro = event->getOpenDocuments()->traductPro(itemClicked);
        corre::LanguageDatas::Item item = pro.getItem(option->text().toStdString());
        newFile::instance()->show(event, std::string(option->text().toStdString() + " file").c_str(), item.defaultExtencion.c_str(), pro.getName().c_str(), item.name.c_str(), pro, this);
        return;
    //}
}

void baseProyectView::doMenuOpenFiles(QAction *option)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    //the error managment
    _pbExeption.addFunction("tree::doMenuOpenFiles");
    if (!option)
    {
        _pbExeption << "doMenuOpenFiles";
        _pbExeption << "tree";
        _pbExeption << "the parameter QAction *option = " << option;
        throw _pbExeption;
    }
    //basic data
    QString path = event->getOpenDocuments()->traductPro(itemClicked).getName().c_str();
    //the actions
    if (option->text() == "source files")
    {
        //_openFile->show(event->getOpenDocuments()->traductPro(itemClicked), true, "source");
        return;
    }
    if (option->text() == "header files")
    {
        //_openFile->show(event->getOpenDocuments()->traductPro(itemClicked), true, "header");
        return;
    }
    if (option->text() == "CMake file")
    {
        //_newFile->show("CMAKE file", "CMakeLists.txt", path, "cmake", event->getOpenDocuments()->traductPro(itemClicked));
        return;
    }
    if (option->text() == "Open folder")
    {
        openFiles::instance()->show(event, event->getOpenDocuments()->traductPro(itemClicked), false, "", this);
        return;
    }
    if (option->text() == "file")
    {
        openFiles::instance()->show(event, event->getOpenDocuments()->traductPro(itemClicked), true, "", this);
        return;
    }
    openFiles::instance()->show(event, event->getOpenDocuments()->traductPro(itemClicked), true, option->text().toStdString(), this);
}



void baseProyectView::mousePressEvent(QMouseEvent *event)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    //the error managment
    _pbExeption.addFunction("tree::mousePressEvent");
    if (!event)
    {
        _pbExeption << "mousePressEvent";
        _pbExeption << "tree";
        _pbExeption << "the param QMouseEvent *event is " << event;
        throw _pbExeption;
    }
    //the actions
    if (event->button()== Qt::RightButton)
    {
        menuActive = true;
    }
    else
    {
        menuActive = false;
    }
    QTreeWidget::mousePressEvent(event);
}

void   baseProyectView::itemDoubleClicked(QTreeWidgetItem * item, int)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    switch (event->getOpenDocuments()->type(item))
    {
    case corre::itemType::PROYECT:
        return;
    case corre::itemType::GROUP:
        return;
    }
    corre::Document d = createDocByItem(item);
    d.setLoadFlag(true);
}

 void  baseProyectView::renameItem(QTreeWidgetItem *item, int)
 {
     std::lock_guard<std::recursive_mutex> locker(mu1);
     //the error managment
     _pbExeption.addFunction("tree::renameItem");
     if (!item)
     {
         _pbExeption << "renameItem";
         _pbExeption << "tree";
         _pbExeption << "the parameter QTreeWidgetItem *item = " << item;
         throw _pbExeption;
     }
     //invoque the correct events methods
     switch (event->getOpenDocuments()->type(item))
     {
     case corre::itemType::PROYECT:
         if (noUpdateProItem)
         {
             break;
         }
          event->executeFuncProUpdate(event->getOpenDocuments()->traductPro(item));
          break;
     case corre::itemType::DOCUMENT:
          if (noUpdateDocItem)
          {
              break;
          }            
          event->getOpenDocuments()->traductDoc(item).rename(item->text(0).toStdString());
          //event.executeFuncDocUpdate(event->getOpenDocuments()->traductDoc(item), doc);
          break;
     }
 }
