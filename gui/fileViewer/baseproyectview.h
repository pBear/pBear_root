 /**@file tree.h
 * @author Aron Surian Wolf
 * @bug execute build neet to be connected
 * @brief used to represent in a tree structure the headers, source, ... files
 * @date 25.6.2014
 *
 * The top level items are the proyects. Every proyect has got different containers
 * like "herers", "sources".... the exeptuation are the config files (CMAKE...) this are
 * on the same level as this containers.
 *
 * It is disigned in that wey that every change is make (or is connected to the event manager)
 * so that changes can be reflected in other widgets
**/

#ifndef BASEPROYECTVIEW_H
#define BASEPROYECTVIEW_H

#include "document.h"
#include "event.h"

#include <iostream>
#include <string>
#include <vector>
#include <mutex>

#include <QTreeWidget>
#include <QMenu>
#include <QMouseEvent>
#include <QTextDocument>
#include <QEvent>
#include <QMouseEvent>
#include <QMenu>
#include <QTextDocument>

namespace pBear
{

class baseProyectView : public QTreeWidget
{
    Q_OBJECT
protected:
    //munus
    ///the menu used when righ click on a menu item
    QMenu                     menuProy;
    ///the menu used when clicked on a file item
    QMenu                     menuFile;
    ///a sub menu of the menuProy, used to add (create) documents to it
    QMenu                     menuProyAdd;
    ///a sub menu of the menuProy, used to open a document and add it to the proyect
    QMenu                     menuProyOpen;

    ///used for the menus, to know what of this what used to invoque it;
    QTreeWidgetItem          *itemClicked = NULL;
    ///used for the atribute "itemClicked" to know if the click was on a item and right clicked
    bool                      menuActive;
    ///used to get the interface id of a document or a proyect
    const int                 id;
    ///flag used to avoid the itedm edit excute upddateDocument;
    bool                      noUpdateDocItem = false;
    ///flag used to avoid the itedm edit excute upddateProyect
    bool                      noUpdateProItem = false;
    ///used for threat safety
    std::recursive_mutex      mu1;
    ///pointer used for events
    corre::Event             *event;
protected:
    virtual corre::Document createDocByItem(DOC doc) = 0;
    void    mouseDoubleClickEvent(QMouseEvent *event);
    void    prepareMenusForProject(corre::Project pro);
    void    prepareMenusForFile(corre::Document doc);
public:
    ///constructor: the standart sed in Qt
    explicit baseProyectView(int id, corre::Event *event, QWidget *parent = 0);
public slots:
    ///used for add a doc item. It is compatible with the event manager
    virtual corre::Event::docData addItem(corre::Document doc)        = 0;
    ///used for add a document. It is compatible with the event manager
    virtual void                  addItem(corre::Project pro)         = 0;
    ///update the proyect data in the widget
    virtual void                  updateProyect(corre::Project pro)   = 0;
    ///update the document data in the widget
    virtual void                  updateDocument(corre::Document doc) = 0;
    ///used to open a document. It is compatible with the event manager
    virtual void                  loadDoc(corre::Document doc)         = 0;
    ///used to close a document
    virtual void                  closeDoc(corre::Document doc)              = 0;
    virtual void                  changeLangnuage(corre::Project pro) = 0;
protected slots:
    ///it is invoqued when a action is clicked on the document menu
    ///it make that this action is realiced in the correct way. Often
    /// it interactuve with the event manager
    void                      doMenuFile(QAction *option);
    ///it is invoqued when a action is clicked on the proyect menu.
    /// It make that this action is realiced in the correct way. Often
    /// it interactuve with the event manager
    void                      doMenuProyect(QAction *option);
    ///It is invoqued when a action is clicked on the add sub menu of
    /// the proyect. It make that this action is realiced in the correct
    /// way. Often it interactue with the event manager
    void                      doMenuAddFile(QAction *option);
    ///It is invoqued when a action is clicked on the open sub menu of
    /// the proyect. It make that this action is realiced in the correct
    /// way. Often it interactue with the event manager
    void                      doMenuOpenFiles(QAction *option);
    ///Invoqued when the mouse is clicked. It is necesary to manage the menus
    /// with the help with itemClick
    void                      mousePressEvent(QMouseEvent *event);
    ///Invoqued when the moose is double clocked. It is necesary to open (load)
    /// ad document when it is double clicked
    void                      itemDoubleClicked(QTreeWidgetItem * item, int);
    ///Invoqued when a item is clicked. With the help of the
    /// mousePressEvent invoque the menu
    virtual void              itemClick(QTreeWidgetItem *item, int) = 0;
    ///used when a item is edited (name)
    virtual void              renameItem(QTreeWidgetItem *item, int);
    ///unload doc
    virtual void              unloadDoc(corre::Document doc) = 0;
};

}
#endif // BASEPROYECTVIEW_H
