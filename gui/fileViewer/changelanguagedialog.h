#ifndef CHANGELANGUAGEDIALOG_H
#define CHANGELANGUAGEDIALOG_H

#include "project.h"

#include <QDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QToolButton>
#include <QListWidgetItem>
#include <QPushButton>

namespace Ui {
class ChangeLanguageDialog;
}

class itemChangeLanguageDialogClose: public QDialog
{
    Q_OBJECT
private:
    QHBoxLayout layout;
    QPushButton buttonClose;
    QPushButton buttonMoveToTrush;

public:
    explicit itemChangeLanguageDialogClose(QWidget *parent);
signals:
    void close();
    void moveToTrush();

};

class itemChangeLanguageDialog: public QWidget
{
    Q_OBJECT
public:
     std::shared_ptr<QHBoxLayout>     layout;
     std::shared_ptr<QLabel>          file;
     std::shared_ptr<QComboBox>       comb;
     std::shared_ptr<QLabel>          note;
     std::shared_ptr<QToolButton>     close;
     pBear::corre::Document                  doc;
     itemChangeLanguageDialogClose   *dia;

public:
    explicit itemChangeLanguageDialog(pBear::corre::Document doc);
    QString  getName();
    void     changeLanguage();
public slots:
    void     end();
    void     closeEvent();
    void     moveToTrushEvent();
    void chageItemType(int);
};

class ChangeLanguageDialog : public QDialog
{
    Q_OBJECT
private:


    Ui::ChangeLanguageDialog *ui;
    pBear::corre::Project            pro;

    //functions
    void setComboBoxData(QString item);
    void setItems();
public:
    explicit ChangeLanguageDialog(QWidget *parent, pBear::corre::Project pro, QString item);
    ~ChangeLanguageDialog();
public slots:
    void accept();
};


#endif // CHANGELANGUAGEDIALOG_H
