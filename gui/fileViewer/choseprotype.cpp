#include "choseprotype.h"
#include "ui_choseprotype.h"
#include "event.h"

using namespace pBear;

choseProType::choseProType(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::choseProType)
{
    ui->setupUi(this);
}

choseProType::~choseProType()
{
    delete ui;
}

void choseProType::show(pBear::corre::Event *event, PROY _pro)
{
    this->event = event;
    this->_pro = _pro;
    if (event->getOpenDocuments()->traductPro(_pro).getLanguage() != "")
    {
        setModal(true);
        setWindowFlags(Qt::Dialog);
        QDialog::show();
    }
}

void choseProType::accept()
{
    _openDocuments->traductPro(_pro).setLanguage(ui->comboBox->currentText().toStdString());
}
