#ifndef CHOSEPROTYPE_H
#define CHOSEPROTYPE_H

#include "project.h"

#include <QDialog>

namespace Ui {
class choseProType;
}

namespace pBear
{

class choseProType : public QDialog
{
    Q_OBJECT
private:
    PROY _pro;
    pBear::corre::Event *event;
public:
    explicit choseProType(QWidget *parent = 0);
    void show(pBear::corre::Event *event, PROY _pro);
    ~choseProType();

private:
    Ui::choseProType *ui;

public slots:
    void accept();
};
}

#endif // CHOSEPROTYPE_H
