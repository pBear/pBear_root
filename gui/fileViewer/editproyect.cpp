#include "ui_editproyect.h"
#include "editproyect.h"
#include "event.h"

#include <QFileDialog>
#include <QPushButton>
#include <boost/regex.hpp>

using namespace pBear;

editProyect *editProyect::once = nullptr;

void editProyect::setComboCompileExecuteCompileMode()
{
    ui->comboBox->clear();
    if (ui->language->currentIndex() < 1 || ui->language->count() < 2) return;

    for (auto x: pBear::corre::Language::instance()->getLanguage(ui->language->currentText().toStdString())->getComandStructure())
    {
        ui->comboBox->addItem(x.name.c_str());
    }

    if (!pro.getLanguage().empty())
    {
        ui->comboBox->setCurrentText(pro.getComandStructure()->name.c_str());
    }
}

editProyect::editProyect(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::editProyect)
{
    ui->setupUi(this);

    connect(ui->search, SIGNAL(clicked()), this, SLOT(search()));
    connect(ui->language, SIGNAL(currentIndexChanged(int)), this, SLOT(currentIndexChanged(int)));
}

editProyect::~editProyect()
{
    delete ui;
}

editProyect *editProyect::instance()
{
    if (!once) once = new editProyect;
    return once;
}

void editProyect::show(corre::Event* event, corre::Project pro, QWidget *widget)
{
    this->pro   = pro;
    this->event = event;
    this->setParent(widget);
    setComboCompileExecuteCompileMode();
    if (pro.getLanguage() != "")
    {
        ui->buttonBox->button(QDialogButtonBox::Ok)->hide();
    }
    else
    {
        ui->buttonBox->button(QDialogButtonBox::Ok)->show();
        ui->language->clear();

        ui->language->addItem("none");
        for (std::pair<std::string, corre::LanguageDatas*> x: *corre::Language::instance())
        {
            ui->language->addItem(x.first.c_str());
            if (x.first == pro.getLanguage())
            {
                ui->language->setCurrentIndex(ui->language->count());
            }
        }
    }

    if (pro.getName().empty())
    {
        ui->name->setText("default");
    }
    else
    {
        ui->name->setText(pro.getName().c_str());
    }

    setModal(true);
    setWindowFlags(Qt::Dialog);
    QDialog::show();
}

void editProyect::search()
{
    ui->name->setText(QFileDialog::getExistingDirectory(this, "path", "."));
}

void editProyect::accept()
{
    std::string path = ui->name->text().toStdString();

    if (boost::regex_search(path, boost::regex("^\\s*/")))
    {
        pro.setName(path);
    }
    else
    {
        pro.setName("");
    }
    pro.setLanguage(ui->language->currentText().toStdString());
    auto data = pBear::corre::Language::instance()->getLanguage(pro.getLanguage())->getComandStructure();
    std::string name = ui->comboBox->currentText().toStdString();
    auto it = std::find_if(data.begin(), data.end(), [name](pBear::corre::LanguageDatas::ComandStructure comandStructure)
    {
           return comandStructure.name == name;
    });
    pro.setComandStructure(*it);
    event->executeFuncProUpdate(pro);
    pBear::corre::Language::instance()->getLanguage(pro.getLanguage())->addDocument(pro);
    QDialog::accept();
}

void editProyect::currentIndexChanged(int index)
{
    switch (index)
    {
    case  0:
        ui->buttonBox->button(QDialogButtonBox::Ok)->hide();
        break;
    default:
        //pro.setLanguage(ui->language->currentText().toStdString());
        ui->buttonBox->button(QDialogButtonBox::Ok)->show();
    }
    setComboCompileExecuteCompileMode();
}

void editProyect::currentIndexCompileExecuteCompileChanged(int index)
{

}
