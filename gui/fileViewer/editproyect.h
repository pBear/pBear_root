#ifndef EDITPROYECT_H
#define EDITPROYECT_H

#include "project.h"
#include "event.h"

#include <QDialog>

namespace Ui {
class editProyect;
}

namespace pBear
{
class editProyect : public QDialog
{
    Q_OBJECT
private:
    Ui::editProyect *ui;
    corre::Project   pro;
    corre::Event    *event;
    static editProyect *once;

    void setComboCompileExecuteCompileMode();
public:
    explicit editProyect(QWidget *parent = 0);
    ~editProyect();
public slots:
    static editProyect *instance();
    void show(corre::Event* event, corre::Project pro, QWidget *widget);
    void search();
    void accept();
    void currentIndexChanged(int index);
    void currentIndexCompileExecuteCompileChanged(int index);
};
}

#endif // EDITPROYECT_H
