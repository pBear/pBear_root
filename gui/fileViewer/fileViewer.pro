#-------------------------------------------------
#
# Project created by QtCreator 2015-08-30T17:39:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = fileViewer
#TEMPLATE = app
TEMPLATE      = lib

QMAKE_CXXFLAGS += -std=c++14 -fPIC -Wl,-R/home/aron/pBear/gui/build-fileViewer-Desktop-Debug

LIBS += -pthread -lX11

SOURCES += widget.cpp \
           baseproyectview.cpp \
           tree.cpp \
    newclass.cpp \
    editproyect.cpp \
    openfiles.cpp \
    changelanguagedialog.cpp \
    newfile.cpp \
    choseprotype.cpp
 #main.cpp\

HEADERS  += widget.h \
            baseproyectview.h \
            tree.h \
    newclass.h \
    editproyect.h \
    openfiles.h \
    changelanguagedialog.h \
    newfile.h \
    choseprotype.h

FORMS    += widget.ui \
    dialog.ui \
    editproyect.ui \
    openfiles.ui \
    changelanguagedialog.ui \
    newfile.ui \
    choseprotype.ui

LIBS += -L../../libs -lpBear_corre -lboost_filesystem -lboost_system -lboost_regex -lpBear_executedebugcompileterminal

INCLUDEPATH += ../../corre/include ../../ExecuteDebugCompileTerminal ../../compiler ../../Execute ../../debuger /home/aron/pBear/TerminalProcess/client/
DEPENDPATH  += ../../libs

LIBS += -L/home/aron/pBear/libs   -lboost_system -lboost_thread -lpthread -lboost_serialization -lpBear_termical_process -lpBear_execute -lpBear_debuger
LIBS += -lpBear_compiler -lpBear_corre -lboost_regex -lpthread  -lboost_regex -lpBear_termical_process -lboost_system -lboost_thread -lpthread -lboost_serialization

OTHER_FILES += \
    fileViewer.pro.user
