#ifndef DIALOG_H
#define DIALOG_H

#include "project.h"
#include "event.h"

#include <QDialog>

namespace Ui {
class Dialog;
}

using namespace pBear;

class newClass : public QDialog
{
    Q_OBJECT
private:
    QString head_bef;
    QString source_bef;

public:
    explicit newClass(corre::Event *event, QWidget *parent = 0);
    ~newClass();

private:
    Ui::Dialog    *ui;
    corre::Project pro;
    corre::Event  *event;
private slots:
    void accept();
    void head_changed(QString);
    void source_changed(QString name);
    void sourceHeadFind();
    void headFind();
    void sourceFind();
public slots:
    void show(corre::Project pro);
signals:
    void ok(QString head,QString source);
};

#endif // DIALOG_H
