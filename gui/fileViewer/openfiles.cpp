#include "openfiles.h"
#include "ui_openfiles.h"
#include "event.h"
#include "project.h"
#include "language.h"
#include "changelanguagedialog.h"

openFiles *openFiles::once = nullptr;

pBear::corre::Event *_event;

item::item(QListWidgetItem* _it, std::string itemName, openFiles *par):
    it(_it),itemName(itemName), par(par)
{
    layout = new QHBoxLayout(this);
    file   = new QLabel;
    comb   = new QComboBox;
    note   = new QLabel;
    search = new QToolButton;
    close  = new QToolButton;;

    search->setText("...");
    search->setStyleSheet(" border: 0px");

    close->setStyleSheet("border: 0px");
    close->setText("X");



    note->setStyleSheet("color: red");

    layout->addWidget(file);
    layout->addWidget(comb);
    layout->addWidget(note);
    layout->addWidget(search);
    layout->addWidget(close);


    layout->setMargin(0);

    connect(close,SIGNAL(clicked()),this,SLOT(end()));
    connect(search,SIGNAL(clicked()),this,SLOT(searchF()));
}

QSize item::getSize() {
    return QSize(100,20);
}

void   item::setFile(QString name)
{
    file->setText(name);
    if(_event->getOpenDocuments()->existDocByName(name.toStdString()))
        note->setText("Already load");
    else
        note->setText("");

    changeLanguage();
}

QString  item::getName() {
    return file->text();
}

void item::addDoc()
{
    pBear::corre::Document doc("");

    if (itemName == "" and comb->currentIndex() == 0)
    {
        return;
    }

    doc.setName(getName().toStdString());
    doc.setProyect(par->pro);
    std::cout << comb->currentText().toStdString() << std::endl;
    doc.setItem(comb->currentText().toStdString());
}

void item::changeLanguage()
{
    std::string im;
    comb->clear();
    if (par->pro.getLanguage() == "")
    {
        comb->addItem("general");
    }
    else if (par->itemName == "")
    {
        comb->addItem("none");
        im = par->pro.getFileType(this->getName().toStdString());
        int index = 0;
        int i = 0;
        for (pBear::corre::LanguageDatas::Item item: par->pro.getItems())
        {
            index++;
            comb->addItem(item.name.c_str());
            if (im == item.name)
            {
                i = index;
            }
        }
        comb->setCurrentIndex(i);
    }
    else
    {
        comb->addItem(itemName.c_str());
        comb->setCurrentIndex(0);
    }


}

void item::searchF() {
    setFile(QFileDialog::getOpenFileName(this,"files to load",".",par->toSelectFileStr(itemName)));
}

void item::end() {
    delete it;
    QWidget::close();
}

openFiles::openFiles(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::openFiles)
{
    ui->setupUi(this);
    connect(ui->pushButton,SIGNAL(clicked()),ui->listWidget,SLOT(clear()));
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(search()));
    connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(currentIndexChanged(int)));
}

void openFiles::openDir(QString path)
{
    //declare variabls
    QString          name;
    vector<string>   tmp;
    QListWidgetItem* it1;
    item*            lab1;

    //set path that will open a directory
    if (path.isEmpty())
    {
        name = QFileDialog::getExistingDirectory(this,"Get the file",".");
    }
    else
    {
        name = path;
    }

    //get all files of a directory
    tmp = event->getOpenDocuments()->redRDir(name.toStdString());

    //add the items
    for(auto x: tmp)
    {
        //create the items
        it1  = new QListWidgetItem;
        lab1 = new item(it1, itemName, this);
        //set data
        lab1->setFile(x.c_str());
        it1->setSizeHint(lab1->getSize());
        //ad the item to the widget
        ui->listWidget->addItem(it1);
        ui->listWidget->setItemWidget(it1,lab1);
    }
}

void openFiles::openSFiles()
{
    //variabls
    QStringList      list = QFileDialog::getOpenFileNames(this,"open files",".",toSelectFileStr(itemName));
    QListWidgetItem* it1  = NULL;
    item*            lab1 = NULL;

    //add
    for(auto x: list)
    {
        //create the items
        it1  = new QListWidgetItem;
        lab1 = new item(it1, itemName, this);
        //set data
        lab1->setFile(x);
        it1->setSizeHint(lab1->getSize());
        //add the item
        ui->listWidget->addItem(it1);
        ui->listWidget->setItemWidget(it1,lab1);
    }
}

openFiles *openFiles::instance()
{
    if (!once) once = new openFiles;
    return once;
}

void openFiles::search(QString path)
{
    //open files individualy
    if(fileFolder)
    {
        openSFiles();
    }
    //open a entraly directory
    else
    {
        openDir(path);
    }
}

void openFiles::accept()
{
    //declaration of variabls
    //pBear::corre::Document         doc("");
    QListWidgetItem* it;

    //add the documents to the proyect
    for(int i = 0; i < ui->listWidget->count(); i++)
    {
        it = ui->listWidget->item(i);
        dynamic_cast<item*>(ui->listWidget->itemWidget(it))->addDoc();
    }

    //clear
    ui->listWidget->clear();

    QDialog::accept();
}

void openFiles::currentIndexChanged(int index)
{
    switch (index)
    {
    case 0:
        ui->buttonBox->button(QDialogButtonBox::Ok)->hide();
        break;
    default:
        pro.setLanguage(ui->comboBox->itemText(index).toStdString());
        ui->buttonBox->button(QDialogButtonBox::Ok)->show();
    }


    QListWidgetItem *it;
    for (int i = 0, n = ui->listWidget->count(); i < n; i++)
    {
        it = ui->listWidget->item(i);
        dynamic_cast<item*>(ui->listWidget->itemWidget(it))->changeLanguage();
    }
}

void openFiles::show(pBear::corre::Event *event, pBear::corre::Project pro, bool fileFolder, std::string itemName,  QWidget *widget)
{
    this->event = _event = event;
    this->setParent(widget);
    //hide inecesesary elements
    // and hide elements that can be made a incorrect use of the program
    if (pro.getLanguage() != "")
    {
        ui->comboBox->hide();
    }
    else
    {
        ui->comboBox->show();
        ui->buttonBox->button(QDialogButtonBox::Ok)->hide();
        ui->comboBox->clear();
        ui->comboBox->addItem("none");
        for (auto x: *pBear::corre::Language::instance())
        {
            ui->comboBox->addItem(x.first.c_str());
        }
    }

    //Set data
    this->pro        = pro;
    this->fileFolder = fileFolder;
    this->itemName   = itemName;

    //because this eould be the action that do a normal user, the program do it for him
    ui->pushButton->click();

    //show the dialog
    setModal(true);
    setWindowFlags(Qt::Dialog);
    QDialog::show();
}

void openFiles::show(pBear::corre::Event *event, pBear::corre::Project pro, QString path, QWidget *widget)
{
    this->event = _event = event;
    this->setParent(widget);
    //hide inecesary elements
    ui->pushButton->hide();
    if (pro.getLanguage() != "")
    {
        ui->comboBox->hide();
    }
    else
    {
        ui->comboBox->show();
        ui->buttonBox->button(QDialogButtonBox::Ok)->hide();
    }

    //flags
    this->fileFolder = false;
    this->itemName   = "";
    this->pro        = pro;

    //search for the directory entries
    search(path);

    //show the dialog
    setModal(true);
    setWindowFlags(Qt::Dialog);
    QDialog::show();
}

openFiles::~openFiles()
{
    delete ui;
}

QString openFiles::toSelectFileStr(std::string itemName) {
    std::vector<pBear::corre::LanguageDatas::Item> items = pro.getItems();
    std::string                      data;

    if (pro.getLanguage() != "")
    {
        for (pBear::corre::LanguageDatas::Item extens: items)
        {
            if (extens.name == itemName or itemName == "")
            {
                data = itemName + " (";
                for (std::string x: extens.extencions)
                {
                    data += "*" + x + " ";
                }
                data += ");; ";
            }
        }
    }
    data += "all (*)";
    return data.c_str();
}
