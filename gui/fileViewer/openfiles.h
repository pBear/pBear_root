#ifndef OPENFILES_H
#define OPENFILES_H

#include "project.h"
#include "event.h"

#include <QDialog>
#include <QFileDialog>
#include <QWidget>
#include <QHBoxLayout>
#include <QComboBox>
#include <QLabel>
#include <QToolButton>
#include <iostream>
#include <QFileDialog>
#include <vector>
#include <QListWidgetItem>

using std::vector;


namespace Ui {
class openFiles;
}

class openFiles;

class item: public QWidget
{
    Q_OBJECT
public:
     QHBoxLayout*     layout;
     QLabel*          file;
     QComboBox*       comb;
     QLabel*          note;
     QToolButton*     search;
     QToolButton*     close;
     QListWidgetItem* it;
     std::string      itemName;
     openFiles       *par;

public:
    explicit item(QListWidgetItem* _it, std::string itemName, openFiles *par);
    QSize    getSize();
    void     setFile(QString);
    QString  getName();
    void     changeLanguage();
    void     addDoc();
private slots:
    void     searchF();
    void     end();
};

class openFiles : public QDialog
{
    Q_OBJECT
public:

    ///interface
    Ui::openFiles  *ui;
    ///search mode
    bool            fileFolder;
    ///the file type to load
    std::string     itemName;
    ///the proyect to that this files should be loaded
    pBear::corre::Project  pro;

    ///load all the files of a diretory
    void     openDir(QString path);
    ///load specific files
    void     openSFiles();
    ///object for singleton pattern
    static openFiles *once;
    ///Event
    pBear::corre::Event *event;
public:
    ///singleton method
    static openFiles *instance();
    ///the constructer
    explicit openFiles(QWidget *parent = 0);
    ///destructer, used to delete the interface, only invoqued when the program is finished
    ~openFiles();

private slots:
    ///do the search actio, it invoque openDir or openSFiles
    void     search(QString path = "");
    ///invoque the correct event actions to add the files (executeAddDoc)
    void     accept();
    ///cancel button
       //
    ///change language. here it is necesary to set this to the proyect and shoe and hide elements
    void     currentIndexChanged(int index);
public slots:
    ///invoqued to use the object. consider that the obeject is created only once time: no knowing directroy
    void     show(pBear::corre::Event *event, pBear::corre::Project pro, bool fileFolder, std::string itemName, QWidget *widget);
    ///invoqued to use the object. consider that the obeject is created only once time: open a knowning folder
    void     show(pBear::corre::Event *event, pBear::corre::Project pro, QString path, QWidget *widget);
    QString toSelectFileStr(std::string itemName);
};

#endif // OPENFILES_H
