/********************************************************************************
** Form generated from reading UI file 'changelanguagedialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHANGELANGUAGEDIALOG_H
#define UI_CHANGELANGUAGEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ChangeLanguageDialog
{
public:
    QVBoxLayout *verticalLayout;
    QComboBox *comboBox;
    QListWidget *listWidget;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *ChangeLanguageDialog)
    {
        if (ChangeLanguageDialog->objectName().isEmpty())
            ChangeLanguageDialog->setObjectName(QStringLiteral("ChangeLanguageDialog"));
        ChangeLanguageDialog->resize(400, 300);
        verticalLayout = new QVBoxLayout(ChangeLanguageDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        comboBox = new QComboBox(ChangeLanguageDialog);
        comboBox->setObjectName(QStringLiteral("comboBox"));

        verticalLayout->addWidget(comboBox);

        listWidget = new QListWidget(ChangeLanguageDialog);
        listWidget->setObjectName(QStringLiteral("listWidget"));

        verticalLayout->addWidget(listWidget);

        buttonBox = new QDialogButtonBox(ChangeLanguageDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(ChangeLanguageDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ChangeLanguageDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ChangeLanguageDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ChangeLanguageDialog);
    } // setupUi

    void retranslateUi(QDialog *ChangeLanguageDialog)
    {
        ChangeLanguageDialog->setWindowTitle(QApplication::translate("ChangeLanguageDialog", "Dialog", 0));
    } // retranslateUi

};

namespace Ui {
    class ChangeLanguageDialog: public Ui_ChangeLanguageDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHANGELANGUAGEDIALOG_H
