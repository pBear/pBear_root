/********************************************************************************
** Form generated from reading UI file 'choseprotype.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHOSEPROTYPE_H
#define UI_CHOSEPROTYPE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>

QT_BEGIN_NAMESPACE

class Ui_choseProType
{
public:
    QGridLayout *gridLayout;
    QComboBox *comboBox;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *choseProType)
    {
        if (choseProType->objectName().isEmpty())
            choseProType->setObjectName(QStringLiteral("choseProType"));
        choseProType->resize(400, 80);
        gridLayout = new QGridLayout(choseProType);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        comboBox = new QComboBox(choseProType);
        comboBox->setObjectName(QStringLiteral("comboBox"));

        gridLayout->addWidget(comboBox, 0, 0, 1, 1);

        buttonBox = new QDialogButtonBox(choseProType);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 1, 0, 1, 1);


        retranslateUi(choseProType);
        QObject::connect(buttonBox, SIGNAL(accepted()), choseProType, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), choseProType, SLOT(reject()));

        QMetaObject::connectSlotsByName(choseProType);
    } // setupUi

    void retranslateUi(QDialog *choseProType)
    {
        choseProType->setWindowTitle(QApplication::translate("choseProType", "Dialog", 0));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("choseProType", "c", 0)
         << QApplication::translate("choseProType", "c++", 0)
        );
    } // retranslateUi

};

namespace Ui {
    class choseProType: public Ui_choseProType {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHOSEPROTYPE_H
