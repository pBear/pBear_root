/********************************************************************************
** Form generated from reading UI file 'newfile.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NEWFILE_H
#define UI_NEWFILE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_newFile
{
public:
    QVBoxLayout *verticalLayout;
    QComboBox *comboBox;
    QLineEdit *lineEdit;
    QHBoxLayout *horizontalLayout;
    QLineEdit *lineEdit_2;
    QToolButton *toolButton;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *newFile)
    {
        if (newFile->objectName().isEmpty())
            newFile->setObjectName(QStringLiteral("newFile"));
        newFile->resize(400, 113);
        verticalLayout = new QVBoxLayout(newFile);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        comboBox = new QComboBox(newFile);
        comboBox->setObjectName(QStringLiteral("comboBox"));

        verticalLayout->addWidget(comboBox);

        lineEdit = new QLineEdit(newFile);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        verticalLayout->addWidget(lineEdit);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        lineEdit_2 = new QLineEdit(newFile);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));

        horizontalLayout->addWidget(lineEdit_2);

        toolButton = new QToolButton(newFile);
        toolButton->setObjectName(QStringLiteral("toolButton"));

        horizontalLayout->addWidget(toolButton);


        verticalLayout->addLayout(horizontalLayout);

        buttonBox = new QDialogButtonBox(newFile);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(newFile);
        QObject::connect(buttonBox, SIGNAL(accepted()), newFile, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), newFile, SLOT(reject()));

        QMetaObject::connectSlotsByName(newFile);
    } // setupUi

    void retranslateUi(QDialog *newFile)
    {
        newFile->setWindowTitle(QApplication::translate("newFile", "Dialog", 0));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("newFile", "c", 0)
         << QApplication::translate("newFile", "c++", 0)
        );
        toolButton->setText(QApplication::translate("newFile", "...", 0));
    } // retranslateUi

};

namespace Ui {
    class newFile: public Ui_newFile {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NEWFILE_H
