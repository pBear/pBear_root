/********************************************************************************
** Form generated from reading UI file 'openfiles.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPENFILES_H
#define UI_OPENFILES_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_openFiles
{
public:
    QVBoxLayout *verticalLayout;
    QComboBox *comboBox;
    QPushButton *pushButton;
    QListWidget *listWidget;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *openFiles)
    {
        if (openFiles->objectName().isEmpty())
            openFiles->setObjectName(QStringLiteral("openFiles"));
        openFiles->resize(377, 271);
        verticalLayout = new QVBoxLayout(openFiles);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        comboBox = new QComboBox(openFiles);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setMaximumSize(QSize(16777215, 20));

        verticalLayout->addWidget(comboBox);

        pushButton = new QPushButton(openFiles);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setMaximumSize(QSize(16777215, 20));

        verticalLayout->addWidget(pushButton);

        listWidget = new QListWidget(openFiles);
        listWidget->setObjectName(QStringLiteral("listWidget"));

        verticalLayout->addWidget(listWidget);

        buttonBox = new QDialogButtonBox(openFiles);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(openFiles);
        QObject::connect(buttonBox, SIGNAL(accepted()), openFiles, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), openFiles, SLOT(reject()));

        QMetaObject::connectSlotsByName(openFiles);
    } // setupUi

    void retranslateUi(QDialog *openFiles)
    {
        openFiles->setWindowTitle(QApplication::translate("openFiles", "Dialog", 0));
        pushButton->setText(QApplication::translate("openFiles", "Open files", 0));
    } // retranslateUi

};

namespace Ui {
    class openFiles: public Ui_openFiles {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPENFILES_H
